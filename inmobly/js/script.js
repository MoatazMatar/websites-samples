﻿(function(){
"user strict";
var inMoblyApp = angular.module('inMoblyApp', ['ui.grid','ui.grid.resizeColumns','ui.grid.edit','ui.grid.moveColumns','ui.grid.selection','ui.grid.pagination','ui.grid.autoResize','ui.grid.treeView','ui.grid.pinning','ui.grid.cellNav','ui.grid.rowEdit' ]);
inMoblyApp.controller('baseController', ['$scope', '$rootScope', '$http', '$filter', '$rootScope', '$timeout', 'i18nService', 'uiGridConstants', '$window', baseController]);

function baseController($scope, $rootScope, $http, $filter, $rootScope, $timeout, i18nService, uiGridConstants, $window) {
    var vm = this; 
    var datayou2;

    var getVidDetails = function (i) {
        if (datayou2[i].snippet.title == "Private video") { }
        else {
            if (angular.isUndefined(datayou2[i].contentDetails)) {
                var vidId = datayou2[i].id.videoId;
            }
            else {
                var vidId = datayou2[i].contentDetails.videoId;
            }
            datayou2[i].statistics = { "viewCount": "", "likeCount": "", "dislikeCount": "", "duration": "" }; 
            $http.get("https://www.googleapis.com/youtube/v3/videos?part=statistics%2Csnippet%2CcontentDetails&id=" + vidId + "&key=AIzaSyAQoKSbeBjzfEkXccfrbP_dLlkel7twHLQ").then(function (response2) {
                
                datayou2[i].statistics.viewCount = response2.data.items[0].statistics.viewCount;
                datayou2[i].statistics.likeCount = response2.data.items[0].statistics.likeCount;
                datayou2[i].statistics.dislikeCount = response2.data.items[0].statistics.dislikeCount;
                var Time = response2.data.items[0].contentDetails.duration.split("PT");
                var MinSec = Time[1].split("M");
                var Min = Time[1].split("M")[0];
                var Sec = (MinSec[1].split("S"))[0];
                datayou2[i].statistics.duration = Min+" : "+Sec;
            });
        }
    }

    $http.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&maxResults=50&playlistId=PLEywN28C5m6plsD8ym3Q7WQDKO4ZFXhpO&key=AIzaSyAQoKSbeBjzfEkXccfrbP_dLlkel7twHLQ").then(function (response) {
        
        datayou2 = response.data.items;
        for (var i = 0; i < (datayou2.length - 2) ; i++) {
            getVidDetails(i);
            if (i == (response.data.items.length - 1)) {
                datayou2 = response.data.items;
            }
        }
        $scope.gridOptions2.data = datayou2;
        if (angular.isUndefined($scope.gridOptions2.data[0].contentDetails)) {
            vm.videoPath = $scope.gridOptions2.data[0].id.videoId;
        }
        else {
            vm.videoPath = $scope.gridOptions2.data[0].contentDetails.videoId;
        }
        vm.videoMedThumUrl = $scope.gridOptions2.data[0].snippet.thumbnails.medium.url;
        vm.videoHighThumUrl = $scope.gridOptions2.data[0].snippet.thumbnails.high.url;
        $('.vidHolder').find('iframe').attr('src', 'https://www.youtube.com/embed/' + vm.videoPath + '?rel=0');
    });

    function rowTemplate(row) {
        return '<div ng-click="grid.appScope.rowClick(row)" ng-dblClick="grid.appScope.dbClick(row)" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'custom\': true }" ui-grid-cell></div>'
    }
    $scope.rowClick = function (row) {
        debugger
        if (angular.isUndefined(row.entity.contentDetails)) {
            vm.videoPath = row.entity.id.videoId;
        }
        else {
            vm.videoPath = row.entity.contentDetails.videoId;
        }
        vm.videoMedThumUrl = row.entity.snippet.thumbnails.medium.url;
        vm.videoHighThumUrl = row.entity.snippet.thumbnails.high.url;
        $('.vidHolder').find('iframe').attr('src', 'https://www.youtube.com/embed/' + vm.videoPath + '?rel=0');
    }

    $scope.gridOptions2 = {
        enableSorting: true,
        enableFiltering: false,
        enableGridMenu: false,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        showGridFooter: false,
        showColumnFooter: true,
        paginationPageSizes: [15, 30, 50],
        paginationPageSize: 15,
        enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
        enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
        rowTemplate: rowTemplate(),
        onRegisterApi: function (gridApi) {
            $scope.grid2Api = gridApi;
        },
        columnDefs: [
            { name: 'snippet.title', enableCellEdit: false, displayName: 'Title', width: "25%" },
            { name: 'contentDetails.videoPublishedAt', enableCellEdit: false, displayName: 'Upload Date', width: "12%", cellFilter: 'date:\'dd-MM-yyyy\'' },
            { name: 'statistics.duration', enableCellEdit: false, displayName: 'Duration', width: "10%" },
            { name: 'statistics.viewCount', enableCellEdit: false, displayName: 'No. of views', width: "12%" },
            { name: 'statistics.likeCount', enableCellEdit: false, displayName: 'No. of likes', width: "11%" },
            { name: 'snippet.description', enableCellEdit: false, displayName: 'Description', width: "30%" },
        ]
    };

    vm.changePlayList = function () {
        $http.get("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&maxResults=50&playlistId=" + vm.newPlayList.split("?list=")[1] + "&key=AIzaSyAQoKSbeBjzfEkXccfrbP_dLlkel7twHLQ").then(function (response) {
            
            datayou2 = response.data.items;
            for (var i = 0; i < (datayou2.length-2) ; i++) {
                getVidDetails(i);
                if (i == (response.data.items.length - 1)) {
                    datayou2 = response.data.items;
                }
            }
            $scope.gridOptions2.data = datayou2;
            vm.videoPath = $scope.gridOptions2.data[0].contentDetails.videoId;
            vm.videoMedThumUrl = $scope.gridOptions2.data[0].snippet.thumbnails.medium.url;
            vm.videoHighThumUrl = $scope.gridOptions2.data[0].snippet.thumbnails.high.url;
            $('.vidHolder').find('iframe').attr('src', 'https://www.youtube.com/embed/' + vm.videoPath + '?rel=0');
        });
    }

    vm.videoThumCopy = function (inputId) {
        var copyText = document.getElementById(inputId);
        copyText.select();
        document.execCommand("Copy");
    }
    
    vm.downVidThum = function (inputId) {
        $window.open(document.getElementById(inputId).value, '_blank');
    }

    function setColumnVisibility() {
        if (!angular.isUndefined($scope.gridOptions2)) {
            if ($(window).width() <= 750) {
                $scope.gridOptions2.columnDefs[1].visible = false;
                $scope.gridOptions2.columnDefs[3].visible = false;
                $scope.gridOptions2.columnDefs[4].visible = false;
                $scope.gridOptions2.columnDefs[5].visible = false;
                $scope.gridOptions2.columnDefs[0].width = "80%";
                $scope.gridOptions2.columnDefs[2].width = "20%";
            }
            else if ($(window).width() <= 970) {
                $scope.gridOptions2.columnDefs[1].visible = false;
                $scope.gridOptions2.columnDefs[4].visible = false;
                $scope.gridOptions2.columnDefs[0].width = "37%";
                $scope.gridOptions2.columnDefs[5].width = "42%";
            }
            setTimeout(function () {
                var gridVH = $('.GridData').outerHeight(true);
                var vidVH = $('.videoShow').outerHeight(true);
                $('.vidDet').height($('.vidDet').outerHeight(true) + (gridVH - vidVH - 12));
            }, 1);
        }
    }
    setColumnVisibility();
    $(window).resize(function () {
        setColumnVisibility();
    });

    vm.searchFn = function (txt) {debugger
        if (txt != null && txt != undefined) {
            var channelId = $scope.gridOptions2.data[0].snippet.channelId;
            $http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&channelId="+channelId+"&maxResults=50&q=" + txt + "&key=AIzaSyAQoKSbeBjzfEkXccfrbP_dLlkel7twHLQ").then(function (response) {
                console.log(response);
                $scope.gridOptions2.data = response.data.items;
                debugger

                datayou2 = response.data.items;
                for (var i = 0; i < (datayou2.length - 2) ; i++) {
                    getVidDetails(i);
                    if (i == (response.data.items.length - 1)) {
                        datayou2 = response.data.items;
                    }
                }
                $scope.gridOptions2.data = datayou2;
                vm.videoPath = $scope.gridOptions2.data[0].id.videoId;
                vm.videoMedThumUrl = $scope.gridOptions2.data[0].snippet.thumbnails.medium.url;
                vm.videoHighThumUrl = $scope.gridOptions2.data[0].snippet.thumbnails.high.url;
                $('.vidHolder').find('iframe').attr('src', 'https://www.youtube.com/embed/' + vm.videoPath + '?rel=0');
            });
        }
    }
}

})();