function slideshow1()
	{
		var $slide_active = $('#slideshow IMG.slide_active');
		var imglen = $("#slideshow img");		
		if($slide_active.index() == imglen.length-1)
			{
				var $next = $('#slideshow IMG:eq(0)');
			}
		else
			{
				var $next = $slide_active.next();
			}
		
		$next.addClass("slide_active");
		$slide_active.removeClass("slide_active");
	}

$(function()
	{
		var imglen = $("#slideshow img");
		setInterval("slideshow1()",5000);
		
		for(var i=0; i<imglen.length; i++)
			{
				var bl = '<a href="#" id="pl_'+(i+1)+'">'+(i+1)+'</a>';
				$(bl).appendTo("#slide_num");
			}
			
		$("#slide_num a").click(function()
								{
									var lid = $(this).attr('id').split('_');
									//alert(lid[1]-1);
									$("#slideshow img:visible").removeClass('slide_active');
									$('#slideshow img:eq('+(lid[1]-1)+')').addClass('slide_active');
									return false;
								}
							);
		
		$("#prev").click(function()
								{
									var s = $("#slideshow img:visible").index();
									if(s == 0)
										{
											$("#slideshow img:visible").removeClass('slide_active');
											$('#slideshow img:last').addClass('slide_active');
										}
									else
										{
											$("#slideshow img:visible").removeClass('slide_active');
											$('#slideshow img:eq('+(s-1)+')').addClass('slide_active');
										}
										
									return false;
								}
							);
							
		$("#next").click(function()
								{
									var s = $("#slideshow img:visible").index();
									if(s == (imglen.length-1))
										{
											$("#slideshow img:visible").removeClass('slide_active');
											$('#slideshow img:first').addClass('slide_active');
										}
									else
										{
											$("#slideshow img:visible").removeClass('slide_active');
											$('#slideshow img:eq('+(s+1)+')').addClass('slide_active');
										}
										
									return false;
								}
							);
							
	}
);