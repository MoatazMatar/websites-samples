$(function()
	{
		$("#setTour h2").click(function()
										{
											$("#setTour div").toggle();
											var tourone = '<div id="tour"><div id="tourseo"><span id="tpagetitle"></span><span id="tpagedesc"></span><span id="tpagekey"></span><span id="tpageurl"></span></div><div id="tourinfo"><span id="ttitle"></span><span id="tdays"></span><span id="tprice"></span></div><div id="tourhomeinfo"><span id="thpagetitle"></span><span id="thpagedesc"></span></div><div id="tourmaindesc"><span id="t_1_title"></span><span id="t_1_desc"></span></div><div id="tourdays"><div id="td_1_info"><span id="td_1_title__1"></span><span id="td_1_desc__1"></span></div></div><div id="touroptional"></div><div id="tourgallery"><span id="tg_1_title"></span><span id="tg_1_desc"></span><span id="tg_1_src"></span></div></div>';
											$(tourone).appendTo('body');
										}
									);
									
		$('#tourhometitle').change(function()
										{
											$('#tpagetitle').html($('#tourhometitle').val());
										}
									);
									
		$('#tourhomedesc').change(function()
										{
											$('#tpagedesc').html($('#tourhomedesc').val());
										}
									);
									
		$('#tourhomekey').change(function()
										{
											$('#tpagekey').html($('#tourhomekey').val());
										}
									);
									
		$('#tourhomeurl').change(function()
										{
											$('#tpageurl').html($('#tourhomeurl').val());
										}
									);
									
		$('#tourtitle').change(function()
										{
											$('#ttitle').html($('#tourtitle').val());
										}
									);
									
			//tour days	is missing	///////////	
									
		$('#tourregularprice').change(function()
											{
												$('#tprice').html($('#tourregularprice').val());
											}
										);
									
		$('#tourhomepagetitle').change(function()
											{
												$('#thpagetitle').html($('#tourhomepagetitle').val());
											}
										);
										
		$('#tourhomepageshortdesc').change(function()
												{
													$('#hpagedesc').html($('#tourhomepageshortdesc').val());
												}
											);
											
											
				//tour main body edit begin ///
			
		$('#tourmaintitle_1').change(function()
											{
												$('#t_1_title').html($('#tourmaintitle_1').val());
											}
										);
										
		$('#tourmaindesc_1').change(function()
											{
												$('#t_1_desc').html($('#tourmaindesc_1').val());
											}
										);
									
									
									
		var n=2;
		$("#tourDescription").click(function()
											{
												var tdesc = '<label>tour main title:</label><input type="text" id="tourmaintitle_'+n+'" size="40" /><br /><label>tour main description:</label><textarea cols="40" rows="4" warp="soft" id="tourmaindesc_'+n+'" ></textarea><br />';
												$(tdesc).insertBefore("#tourDescription");
												tourmainbody(n);
												n++;												
												return false;
											}
										);
										
		function tourmainbody(i)
			{
				var l = '<span id="t_'+i+'_title"></span><span id="t_'+i+'_desc"></span>';
				$(l).appendTo('#tourmaindesc');				
				$('#tourmaintitle_'+i+'').change(function()
													{
														$('#t_'+i+'_title').html($('#tourmaintitle_'+i+'').val());
													}
												);
										
				$('#tourmaindesc_'+i+'').change(function()
													{
														$('#t_'+i+'_desc').html($('#tourmaindesc_'+i+'').val());
													}
												);
			}
				
				//tour main body edit end ///
				
				
			
				//tour day edit begin ///
			
					//day1 begin ////
			
		$('#td_1_title_1').change(function()
										{
											$('#td_1_title__1').html($('#td_1_title_1').val());
										}
									);
									
		$('#td_1_desc_1').change(function()
										{
											$('#td_1_desc__1').html($('#td_1_desc_1').val());
										}
									);
		
		
		var d = 1;							
		var hh = 2;
		$('#tourdaydesc'+d+'').click(function()
										{
											var ddesc = '<label>tour day another title:</label><input type="text" size="40"  id="td_1_title_'+hh+'" /><br /><label>tour day another description:</label><textarea cols="40" rows="4" warp="soft" id="td_1_desc_'+hh+'" ></textarea><br />';
											$(ddesc).insertBefore(this);
											tourdaybody(hh);
											hh++;
											return false;
										}
									);
									
		function tourdaybody(i)
			{
				var l = '<span id="td_1_title__'+i+'"></span><span id="td_1_desc__'+i+'"></span>';
				$(l).appendTo('#td_1_info');
				$('#td_1_title_'+i+'').change(function()
												{
													$('#td_1_title__'+i+'').html($('#td_1_title_'+i+'').val());
												}
											);
									
				$('#td_1_desc_'+i+'').change(function()
												{
													$('#td_1_desc__'+i+'').html($('#td_1_desc_'+i+'').val());
												}
											);
			}
									
					//day1 end ///
		
		
		
		$("#tourday").click(function()
									{
										var a = $('#setTour #day:last').html().split(' ');
										if (a[2] == '1')var i=2;
										if (a[2] == '2')var i=3;
										if (a[2] == '3')var i=4;
										if (a[2] == '4')var i=5;
										if (a[2] == '5')var i=6;
										if (a[2] == '6')var i=7;
										if (a[2] == '7')var i=8;
										if (a[2] == '8')var i=9;
										if (a[2] == '9')var i=10;
										var s = '<label id="day">tour day '+i+' title:</label><input type="text" size="40" id="td_'+i+'_title_1"/><br /><label>tour day '+i+' description:</label><textarea cols="40" rows="4" warp="soft" id="td_'+i+'_desc_1"></textarea><br /><button id="tourdaydesc'+i+'">add tour day '+i+' description</button><br /><br />';
										$(s).insertBefore("#tourday");
										var ss = '<div id="td_'+i+'_info"><span id="td_'+i+'_title__1"></span><span id="td_'+i+'_desc__1"></span></div>';
										$(ss).appendTo('#tourdays');
										dd(i);
										return false;
									}
								);
								
															
		function dd(i)
			{
				var dh = 2;	
				$('#tourdaydesc'+i+'').click(function()
										{
											var ddesc = '<label>tour day another title:</label><input type="text" size="40"  id="td_'+i+'_title_'+dh+'" /><br /><label>tour day another description:</label><textarea cols="40" rows="4" warp="soft" id="td_'+i+'_desc_'+dh+'" ></textarea><br />';
											$(ddesc).insertBefore(this);
											atourdaybody(i,dh);
											dh++;
											return false;
										}
									);
									
				$('#td_'+i+'_title_1').change(function()
															{
																$('#td_'+i+'_title__1').html($('#td_'+i+'_title_1').val());
															}
														);
									
				$('#td_'+i+'_desc_1').change(function()
															{
																$('#td_'+i+'_desc__1').html($('#td_'+i+'_desc_1').val());
															}
														);
									
				function atourdaybody(a,b)
					{
						var l = '<span id="td_'+a+'_title__'+b+'"></span><span id="td_'+a+'_desc__'+b+'"></span>';
						$(l).appendTo('#td_'+a+'_info');
						$('#td_'+a+'_title_'+b+'').change(function()
															{
																$('#td_'+a+'_title__'+b+'').html($('#td_'+a+'_title_'+b+'').val());
															}
														);
									
						$('#td_'+a+'_desc_'+b+'').change(function()
															{
																$('#td_'+a+'_desc__'+b+'').html($('#td_'+a+'_desc_'+b+'').val());
															}
														);
					}
					
			}
			
				//tour day edit end ///
				
				
		$('#tour_optional').change(function()
										{
											$('#touroptional').html($('#tour_optional').val());
										}
									);
									
		$('#tourimage_1_title').change(function()
											{
												$('#tg_1_title').html($('#tourimage_1_title').val());
											}
										);
										
		$('#tourimage_1_desc').change(function()
											{
												$('#tg_1_desc').html($('#tourimage_1_desc').val());
											}
										);
										
		$('#tourimage_1_src').change(function()
											{
												$('#tg_1_src').html($('#tourimage_1_src').val());
											}
										);
		
		var imin = 2;
		$("#tourimage").click(function()
									{
										var i = '<label></label><input size="40" type="text" value="image title" id="tourimage_'+imin+'_title"  /><br /><label></label><textarea cols="40" rows="4" warp="soft" id="tourimage_'+imin+'_desc">image description</textarea><br /><label></label><input type="file" size="40" id="tourimage_'+imin+'_src"/><br/>';
										$(i).insertBefore("#tourimage");
										addimg(imin);
										imin++;
										return false;
									}
								);
								
		function addimg(a)
			{
				var l = '<span id="tg_'+a+'_title"></span><span id="tg_'+a+'_desc"></span><span id="tg_'+a+'_src"></span>';
				$(l).appendTo('#tourgallery');
				$('#tourimage_'+a+'_title').change(function()
													{
														$('#tg_'+a+'_title').html($('#tourimage_'+a+'_title').val());
													}
												);
										
				$('#tourimage_'+a+'_desc').change(function()
													{
														$('#tg_'+a+'_desc').html($('#tourimage_'+a+'_desc').val());
													}
												);
										
				$('#tourimage_'+a+'_src').change(function()
													{
														$('#tg_'+a+'_src').html($('#tourimage_'+a+'_src').val());
													}
												);
			}

	}
);