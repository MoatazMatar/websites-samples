<html>

<head>

	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/bannar.css" type="text/css" />
	<link rel="stylesheet" href="css/centerpart.css" type="text/css" />
	<link rel="stylesheet" href="css/mainmenu.css" type="text/css" />
	<link rel="stylesheet" href="css/leftside.css" type="text/css" />
	<link rel="stylesheet" href="css/tourpref.css" type="text/css" />
	<link rel="stylesheet" href="css/footer.css" type="text/css" />
	
	<script type="text/javascript" src="javascript/jq.js"></script>
	<script type="text/javascript" src="javascript/tourpref.js"></script>
	<script type="text/javascript" src="javascript/login.js"></script>
	<script type="text/javascript" src="javascript/logged.js"></script>
	<script type="text/javascript" src="javascript/adminlogin.js"></script>
	
</head>

<body>
	
	<table id="mainPage" align="center">
		<tr>
			<td>
			
				<!-- links part start -->
					<?php include ('header_bar.php'); ?>
					
					<div id="clear">
					</div>
				<!-- links part end -->
				
				<!-- bannar start -->
					<div id="bannar">
						<img src="images/bannar.gif" />
					</div>
				<!-- bannar end -->
				
				<!-- menu start -->
					<div id="mainMenu">
						main menu
					</div>
				<!-- menu end -->
				
				<!-- main body start -->
				
					<!-- right side start -->
						<div id="leftSide">
							left Side
						</div>
					<!-- right side end -->
					
					<!-- center start -->
						<div id="center">
							
							<h1>
								vision of the company title
							</h1>
							
							<p>
								vision of the company body
							</p>
							
							<div id="tourPref">
								<h2>
									cairo tour
								</h2>
								<table>
									<tr>
										<td id="tourPic" valign="top">
											<img src="images/ariticles_pic_1.gif" alt="#" title="pic desc"/>
										</td>
										<td id="tourDesc" valign="top">
											<p>
												Our most popular tour, and what were famous for! This excursion to Cairo is a must for anyone visiting Egypt. 
												sights of Cairo and less time traveling, t..
											</p>
											<div id="tourPrice">
												<img src="images/price.gif" title="tour price" alt="#" />
												<h3>
													adult: &pound; 190<br />
													child: &pound; 170
												</h3>
											</div>
											<div id="tourReview">
												<img src="images/custmerreview.gif" alt="" title="customer reviews" />
												<h3>
													41
												</h3>
												<p>
													customer reviews
												</p>
											</div>
											<div id="clear">
											</div>
											<div id="tourButton">
													<a href="#" alt="" title="">read more information</a>
													<button>add to my cart</button>												
											</div>
										</td>
									</tr>
								</table>
							</div>
						
							<div id="tourPref">
								<h2>
									cairo tour
								</h2>
								<table>
									<tr>
										<td id="tourPic" valign="top">
											<img src="images/ariticles_pic_1.gif" alt="#" title="pic desc"/>
										</td>
										<td id="tourDesc" valign="top">
											<p>
												Our most popular tour, and what were famous for! This excursion to Cairo is a must for anyone visiting Egypt. 
												sights of Cairo and less time traveling, t..
											</p>
											<div id="tourPrice">
												<img src="images/price.gif" title="tour price" alt="#" />
												<h3>
													adult: &pound; 190<br />
													child: &pound; 170
												</h3>
											</div>
											<div id="tourReview">
												<img src="images/custmerreview.gif" alt="" title="customer reviews" />
												<h3>
													41
												</h3>
												<p>
													customer reviews
												</p>
											</div>
											<div id="clear">
											</div>
											<div id="tourButton">
													<a href="#" alt="" title="">read more information</a>
													<button>add to my cart</button>												
											</div>
										</td>
									</tr>
								</table>
							</div>
						
						</div>
					<!-- center end -->					
				
				<!-- main body end -->
				
				<!-- clear start -->
					<div id="clear">
					</div>
				<!-- clear end -->
				
				<!-- footer start -->
					<div id="footer">
						footer
					</div>
				<!-- footer end -->
			</td>
		</tr>
	</table>
	
</body>

</html>