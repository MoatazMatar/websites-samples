$(function(){
	"use strict";
	
	$('.project-tabs').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$('.project-gallery.active').removeClass('active');
			$('.project-gallery.'+$(this).attr('role')+'').addClass('active');
			return false;
		}
	});
});