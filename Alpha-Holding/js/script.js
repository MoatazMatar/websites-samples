$(function(){
	
	$('a').click(function(){if($(this).attr('href') == '' || $(this).attr('href') == '#' || $(this).attr('href') == ' '){return false;}});
	
	var prohieght;
	if($('div#projects').find('div.pro_item:eq(1)').outerHeight(true) > $('div#projects').find('div.pro_item:eq(0)').outerHeight(true))
		{
			prohieght = $('div#projects').find('div.pro_item:eq(1)').outerHeight(true);
			$('div#projects').find('div.pro_item:eq(0)').css('height',prohieght-22);
		}
	else
		{
			prohieght = $('div#projects').find('div.pro_item:eq(0)').outerHeight(true);
			$('div#projects').find('div.pro_item:eq(1)').css('height',prohieght-22);
		}
	var promenuhieght = (prohieght-$('div#projects').find('div.pro_menu').find('ul').outerHeight(true))/2;
	$('div#projects').find('div.pro_menu').css('height',promenuhieght);
	$('div#projects').find('div.pro_menu').css('padding-top',(prohieght-$('div#projects').find('div.pro_menu').find('ul').outerHeight(true))/2);
	
	/*for adjust the about cont height*/
	// if($('div#about-cont').position() && $('div#about-menu').position())
		// {
			// $('div#about-cont').css('height', $('div#holder').outerHeight(true));
		// }
		
	/*for about div*/
	if($('div#about-cont').position())
		{
			if($('div#about-cont').find('p').length > 3)
				{
					for(var i=0 ; i<3; i++)
						{
							$('div#about-cont').find('p:eq('+i+')').addClass('active');
						}
						
					$('div#about-cont').find('a.more').click(function(){
							for(var i=0; i<$('div#about-cont').find('p').length; i++)
								{
									$('div#about-cont').find('p:eq('+i+')').toggleClass('active');
								}
						});
				}
		}
		
	/*for about div tabs*/
	$('div#about-menu').find('li').click(function(){
			// $(this).parent().parent().find('li').hasClass('active').removeClass('active');
			$('div#about-menu').find('li').removeClass('active');
			$(this).addClass('active');
			if($('div#holder').find('div.'+$(this).attr('id')+'').offset())
				{
					// alert($(this).parent().attr('id'));
					var act = $('div#holder').find('div#about-cont.active');
					var nxt = $('div#holder').find('div.'+$(this).attr('id')+'');
					$(act).animate({opacity: 0},500).removeClass('active');
					$(nxt).animate({opacity: 1},500).addClass('active');
					// $(act).removeClass('active');
					// $(nxt).addClass('active');
				}
			return false;
		});
		
	/*for company page*/
	if($('div.comp-menu').position() && $('div.comp-cont').position())
		{
			var lastScrollTop = 0;
			$(window).scroll(function(event){
										var st = $(this).scrollTop();
										var mx = $('div.comp-cont').height()+460;
										if (st > 661 && st < mx)
											{
												$('div.comp-menu').addClass('top');
												$('div.comp-cont').addClass('top');
											}
										else
											{
												$('div.comp-menu').removeClass('top');
												$('div.comp-cont').removeClass('top');
											}
										lastScrollTop = st;
									});
									
			$('div.comp-menu').find('li').click(function(){
					if($(this).hasClass('active')){}
					else
						{
							$('div.comp-menu').find('li').removeClass('active');
							$(this).addClass('active');
							var id = $(this).attr('id');
							$('div.comp-cont').find('div.active').animate({opacity:0},500).removeClass('active');
							$('div.comp-cont').find('div.'+id+'').animate({opacity:1},500).addClass('active');
						}
				});
				
			$('div.comp-cont').find('div.gallery').find('div').hover(function(){
					$(this).find('div.screen').animate({opacity: 0},300);
					$(this).find('span').addClass('active');
				},function(){
					$(this).find('div.screen').animate({opacity: 1},300);
					$(this).find('span').removeClass('active');
				});
				
			$('div.comp-cont').find('div.gallery').find('div').find('span').click(function(){
					var src = $(this).parent().find('img').attr('src');
					var title = $(this).parent().find('img').attr('title');
					var alt = $(this).parent().find('img').attr('alt');
					var ind = $(this).parent().index();
					$('div#zoom').find('div.cont').find('img').attr('src',src);
					$('div#zoom').find('div.cont').find('img').attr('title',title);
					$('div#zoom').find('div.cont').find('img').attr('alt',alt);
					$('div#zoom').find('div.cont').find('p').text(title);
					$('div#zoom').find('span').text(ind);
					$('div#zoom').css('height',$('html').outerHeight(true));
					$('div#zoom').fadeIn(1000);
					$('div#zoom').find('div.but').css('width',$('div#zoom').find('div.cont').outerWidth(true)-80);
					$('div#zoom').find('div.but').css('margin-top',-($('div#zoom').find('div.cont').outerHeight(true)-40)/3);
					$('div#zoom').find('div.but').css('margin-bottom',($('div#zoom').find('div.cont').outerHeight(true)-40)/3);
					var rih = $('div#zoom').find('div.cont');
					$('div#zoom').find('div.close').css('left',($(rih).width()/2));
				});
				
			$('div#zoom').find('div.cont').find('img').hover(function(){
					$('div#zoom').find('div.but').fadeIn(500);
				});
			
			$('div#zoom').find('div.cont').mouseout(function(){
					//$('div#zoom').find('div.but').fadeOut();
				});
				
			$('div#zoom').find('div.but').find('div').click(function(){
					var ind = parseInt($('div#zoom').find('span').text());
					var i;
					var nind;
					var len = $('div.comp-cont').find('div.gallery').find('div').length/2;
					if($(this).attr('class') == 'prev')
						{
							// alert(ind);
							if(ind == 0)
								{
									nind = len-1;
								}
								
							else
								{
									nind = ind-1;
								}
						}
					else
						{
							// alert(ind);
							// alert(len-1);
							if(ind == len-1)
								{
									nind = 0;
								}
							else
								{
									nind = ind+1;
								}
						}
						
					// alert(ind+' '+nind);
					i = $('div.comp-cont').find('div.gallery').find('div.screen:eq('+nind+')');
					var src = $(i).parent().find('img').attr('src');
					var title = $(i).parent().find('img').attr('title');
					var alt = $(i).parent().find('img').attr('alt');
					ind = nind;
					$('div#zoom').find('div.cont').find('img').attr('src',src);
					$('div#zoom').find('div.cont').find('img').attr('title',title);
					$('div#zoom').find('div.cont').find('img').attr('alt',alt);
					$('div#zoom').find('div.cont').find('p').text(title);
					$('div#zoom').find('span').text(ind);
				});
				
			$('div#zoom').find('div.close').click(function(){
					$('div#zoom').fadeOut(1000);
					$('div#zoom').find('div.cont').find('img').attr('src',' ');
					$('div#zoom').find('div.cont').find('img').attr('title',' ');
					$('div#zoom').find('div.cont').find('img').attr('alt',' ');
					$('div#zoom').find('div.cont').find('p').text(' ');
				});
		}
		
	/*for investment page*/
	if($('div.inv-menu').position() && $('div.inv-cont').position())
		{
			$('div.inv-menu').find('li').click(function(){
					if($(this).hasClass('active')){}
					else
						{
							$(this).parent().find('li.active').removeClass('active');
							$(this).addClass('active');
							var id = $(this).attr('id');
							$('div.inv-cont').find('div.active').slideUp(500,function(){$('div.inv-cont').find('div.active').removeClass('active');});
							$('div.inv-cont').find('div.'+id+'').slideDown(500,function(){$('div.inv-cont').find('div.'+id+'').addClass('active');});
						}
				});
		}
		
	/*for business sectors page*/
	if($('div.bs-menu').position() && $('div.bs-cont').position())
		{
			$('div.bs-menu').find('li').click(function(){
					if($(this).hasClass('active')){}
					else
						{
							$(this).parent().find('li.active').removeClass('active');
							$(this).addClass('active');
							var id = $(this).attr('id');
							// $('div.bs-cont').find('div.active').slideUp().removeClass('active');
							$('div.bs-cont').find('div.active').animate({opacity: 0},300,function(){$('div.bs-cont').find('div.active').removeClass('active');});
							// $('div.bs-cont').find('div.'+id+'').slideDown().addClass('active');
							$('div.bs-cont').find('div.'+id+'').animate({opacity: 1},450,function(){$('div.bs-cont').find('div.'+id+'').addClass('active');});
						}
				});
		}
		
	/*for media center page*/
	if($('div.med-menu').position() && $('div.med-cont').position())
		{
			/*for media menu*/
			$('div.med-menu').find('ul').find('li').click(function(){
					if($(this).hasClass('active')){}
					else
						{
							$('div.med-menu').find('ul').find('li').removeClass('active');
							$(this).addClass('active');
							var id = $(this).attr('id');
							$('div.med-cont').find('div.mitem.active').fadeOut().removeClass('active');
							$('div.med-cont').find('div.mitem.'+id+'').fadeIn().addClass('active');
						}
				});
			
			/*for news tab*/
			if($('div.mitem.news').position())
				{
					$('div.mitem.news').find('div.but').find('span').click(function(){
							var clas = $(this).attr('class');
							var ind = parseInt($('div.mitem.news').find('div.active').index());
							var len = parseInt($('div.mitem.news').find('div.nitem').length);
							if(clas == 'prev')
								{
									if(ind == 0)
										{
											ind = len-1;
										}
									else
										{
											ind = ind-1;
										}
								}
							else
								{
									if(ind == len-1)
										{
											ind = 0;
										}
									else
										{
											ind = ind+1;
										}
								}
							// alert(ind);
							$('div.mitem.news').find('div.active').animate({opacity: 0},500,function(){$('div.mitem.news').find('div.active').removeClass('active');});
							$('div.mitem.news').find('div.nitem:eq('+ind+')').animate({opacity: 1},500,function(){$('div.mitem.news').find('div.nitem:eq('+ind+')').addClass('active');});
						});
				}
			
			/*for gallery tab*/
			if($('div.med-cont').find('div.mitem.gal').position())
				{
					$('div.med-cont').find('div.mitem.gal').find('div.simg').find('li').find('img').click(function(){
							$('div.med-cont').find('div.mitem.gal').find('div.bimg').find('img').attr('src',$(this).attr('src'));
							$('div.med-cont').find('div.mitem.gal').find('div.bimg').find('img').attr('title',$(this).attr('title'));
							$('div.med-cont').find('div.mitem.gal').find('div.bimg').find('img').attr('alt',$(this).attr('alt'));
							$('div.med-cont').find('div.mitem.gal').find('div.bimg').find('p').text($(this).attr('title'));
						});
				}
		}
		
	/*for career*/
	$('div#about-cont.car').find('ul.carmenu').find('li').click(function(){
			$(this).parent().find('li').removeClass('active');
			$(this).addClass('active');
			$('div#about-cont.car').find('div.caritem').removeClass('active');
			$('div#about-cont.car').find('div.caritem.'+$(this).attr('id')+'').addClass('active');
		});
		
	$('div#about-cont.car').find('div.caritem').find('a').click(function(){
			var id = $(this).parent().attr('class').split(' ');
			var job = $('div#about-cont.car').find('ul.carmenu').find('li#'+id+'').text();
			// alert(job);
			$('div#screen').find('ul').find('li:eq(0)').html('<span>job title</span> '+job+'');
			$('div#screen').height($('body').outerHeight('true')-200);
			$('div#screen').find('div.job-apply').css('margin-left',($('body').outerWidth('true')-800)/2);
			$('div#screen').fadeIn();
		});
		
	$('div#screen').find('form').find('input[type=button]').click(function(){
			if($(this).val() == 'exit'){$('div#screen').fadeOut();}
		});

	/*for slider*/
	setInterval("slider()",8000);
		
});

function slider()
	{
		var len = $('div#slider').find('div.slid_item').length;
		var now = $('div#slider').find('div.slid_item.active').index();
		var nind;
		if(now == len-1){nind = 0;}
		else{nind = now+1;}
		// alert(len+' , '+now+' , '+nind);
		$('div#slider').find('div.slid_item.active').animate({opacity:0},1000,function(){$('div#slider').find('div.slid_item.active').removeClass('active');});
		$('div#slider').find('div.slid_item:eq('+nind+')').animate({opacity:1},1000,function(){$('div#slider').find('div.slid_item:eq('+nind+')').addClass('active');});
	}