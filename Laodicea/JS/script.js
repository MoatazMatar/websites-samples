$(function(){
	"use strict";
	$(window).scroll(function(){
		
		if($(window).scrollTop() >= 150){			
			$('.goto-top').show();
		}
		else{
			if($('.goto-top').position()){
				$('.goto-top').hide();
			}
			else{}
		}
	});
	
	$('.goto-top').click(function(){
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 'slow', 'swing');
	});
	
	/*var foot_heght = $('footer').outerHeight();
	$('.map-sec').css('margin-bottom',foot_heght);
	$(window).resize(function(){
		var foot_heght = $('footer').outerHeight();
		$('.map-sec').css('margin-bottom',foot_heght);
	});*/
	
	$(function () {
  		$('[data-toggle="tooltip"]').tooltip();
	});
	
	$('#navbar-about').find('li').find('a').click(function(){
		if($(this).parent().hasClass('active')){}
		else{
			$(this).parent().parent().find('li.active').removeClass('active');
			$(this).parent().addClass('active');
			$('.about-body').find('div.active').slideUp(1000).removeClass('active');
			var cla = $(this).parent().attr('id');
			$('.about-body').find('div.'+cla+'').slideDown(2000).addClass('active');
		}
		return false;
	});
	
	$('.partner-panel-item').find('h2').click(function(){
		if($(this).parent().hasClass('active')){}
		else{
			$('.partners-panels').find('.partner-panel-item.active').find('div.row').slideUp(1000);
			$('.partners-panels').find('.partner-panel-item.active').removeClass('active');
			$(this).parent().find('div.row').slideDown(2000);
			$(this).parent().addClass('active');
		}
	});
	
	var path = window.location.hash.slice(1);
	var path2 = window.location.href;
	var path3 = path2.slice(path2.lastIndexOf('/')+1);
	if(path3.match('^Partners')){
		if($('span#'+path+'').next('div.partner-panel-item').hasClass('active')){}
		else{
			$('.partners-panels').find('.partner-panel-item.active').find('div.row').slideUp(1000);
			$('.partners-panels').find('.partner-panel-item.active').removeClass('active');
			$('span#'+path+'').next('div.partner-panel-item').find('div.row').slideDown(2000);
			$('span#'+path+'').next('div.partner-panel-item').addClass('active');
		}
	}
	
	
});