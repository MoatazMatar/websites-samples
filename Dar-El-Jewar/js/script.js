$(function(){
	"use strict";
	$('.menu-nav').find('ul li').click(function(){
		if($(this).find('ul')){
			if($(this).hasClass('active')){
				$(this).find('ul').slideUp(800).parent().toggleClass('active');
			}
			else{
				$(this).find('ul').slideDown(1000).parent().toggleClass('active');
			}
		}
	});
	
	$('.life-style').find('.close').click(function(){
		$(this).parent().hide(800).removeClass('active');
	});
	
	
	$('.life-style').find('ul li').click(function(){
		var lid = $(this).attr('id');
		if($(this).hasClass('active')){
			if($('.slide-txt').hasClass('active')){}
			else{
				$('.slide-txt').show(800).addClass('active');
				if($('.slide-txt').find('div.active').hasClass(lid)){}
				else{
					$('.slide-txt').find('div.active').slideUp(800).removeClass('active');
					$('.slide-txt').find('div.'+lid+'').slideDown(800).addClass('active');
				}
			}
		}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			
			
			if($('.slide-txt').hasClass('active')){
				if($('.slide-txt').find('div.active').hasClass(lid)){}
				else{
					$('.slide-txt').find('div.active').slideUp(800).removeClass('active');
					$('.slide-txt').find('div.'+lid+'').slideDown(800).addClass('active');
				}
			}
			else{
				$('.slide-txt').show(800).addClass('active');
				if($('.slide-txt').find('div.active').hasClass(lid)){}
				else{
					$('.slide-txt').find('div.active').slideUp(800).removeClass('active');
					$('.slide-txt').find('div.'+lid+'').slideDown(800).addClass('active');
				}
			}
		}
	});
});