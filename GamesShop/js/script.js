$(function(){
	"use strict";
	$('a').click(function(){
		if($(this).attr('href') === '#'){
			return false;
		}
	});
	$('#sideMenu').click(function(){
		$('.menuOverBack').removeClass('slideOutUp').addClass('slideInDown active');
		$('.hiddenMenu').removeClass('slideOutLeft').addClass('slideInLeft active');
	});
	$('.menuOverBack').click(function(){
		$('.menuOverBack').addClass('slideOutUp').removeClass('slideInDown');
		$('.hiddenMenu').addClass('slideOutLeft').removeClass('slideInLeft');
	});
	$('.exit').click(function(){
		$('.menuOverBack').addClass('slideOutUp').removeClass('slideInDown');
		$('.hiddenMenu').addClass('slideOutLeft').removeClass('slideInLeft');
	});
});