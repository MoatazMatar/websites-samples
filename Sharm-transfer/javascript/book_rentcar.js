$(function()
	{
	
				/*******************************************   calender begin    ****************************************************/
		
		var nowday;
		var nowmon;
		var nowyear;
		
		$('#container').find('.rentcar_book').find('img[id^=cal]').click(function()
																	{
																		var id = $(this).attr('id');
																		$('div.cal'+id[3]+'').fadeIn();
																		
																		var nowdate = new Date();
																		nowdate.setDate(nowdate.getDate());
																		$('#curdat'+id[3]+'').val(nowdate);
																		var d = $('#curdat'+id[3]+'').val().split(' ');
																		var yr = nowdate.getFullYear();
																		var s = ''+d[0]+' '+d[1]+' '+d[2]+' '+yr+'';
																		$('#curdat'+id[3]+'').val(s);
																		
																		nowday = d[2];
																		nowmon = d[1];
																		nowyear = yr;
																		
																		if($('div.cal'+id[3]+'').html() == '')
																		{
																			creatcal(id[3],d[1],d[2],d[0],yr);
																		}
																		
																	}
																);
																
		function close()
			{
				$('#container').find('.rentcar_book').find('div.cal').find('div.end').click(function()
																						{
																							var selcal = $(this).parent().attr('class').split(' ');
																							$('div.'+selcal[1]+'').fadeOut();
																						}
																					);
			}
			
			
		function creatcal(id,mon,day,dayname,year)
			{
				//alert(id+' '+day+' ' +mon+' '+year);
				
				var head = '<div class="head"><span style="margin-right:50px;"><img src="images/prev_mon.png" id="back" alt="back" title="back" /></span><span id="mon'+id+'">'+mon+'</span><span id="year'+id+'">'+year+'</span><span style="margin-left:50px;"><img src="images/next_mon.png" id="next" alt="next" title="next" /></span></div>';
				
				var dayn = '<div class="dayname"><div>mo</div><div>tu</div><div>we</div><div>th</div><div>fr</div><div>sa</div><div>su</div></div>';
				
				var clear = '<div class="clear"></div>';
				
				var valid = $('#curdat'+id+'').val().split(' ');
				
				//alert(valid[1]+' '+valid[3]+' ( '+mon+' '+year+' )');
				
				if(valid[1] == mon && valid[3] == year)
					{
						head = '<div class="head"><span style="margin-right:50px; display:inline-block; width:25px;">&nbsp;</span><span id="mon'+id+'">'+mon+'</span><span id="year'+id+'">'+year+'</span><span style="margin-left:50px;"><img src="images/next_mon.png" id="next" alt="next" title="next" /></span></div>';
					}
				
				var empty;
				var count = 0;
				
				var dd = day%7;
					
				switch (dayname)
					{
						case 'Mon':
							empty = 1;
						break;
						
						case 'Tue':
							empty = 2;
						break;
						
						case 'Wed':
							empty = 3;
						break;
						
						case 'Thu':
							empty = 4;
						break;
						
						case 'Fri':
							empty = 5;
						break;
						
						case 'Sat':
							empty = 6;
						break;
						
						case 'Sun':
							empty = 7;
						break;
					}
					
				
					
				if(mon.toLowerCase() == 'jan' || mon.toLowerCase() == 'mar' || mon.toLowerCase() == 'may' || mon.toLowerCase() == 'jul' || mon.toLowerCase() == 'aug' || mon.toLowerCase() == 'oct' || mon.toLowerCase() == 'dec')
					{
						count = 1;
					}
					
				else if(mon.toLowerCase() == 'feb')
					{
						count = 2;
					}
					
				
				if((year%4) == 0)
					{
						if(mon.toLowerCase() == 'feb')
							{
								count = 3;
							}
					}
				
				var days = '<div class="days"></div>';
				
				var bend = '<div class="bend">&nbsp;</div>';
				var end = '<div class="end" title="close">x</div>';
				
				$('div.cal'+id+'').html(head+dayn+clear+days+clear+bend+end);
				
			if((empty-dd)>0)
			{
				for(var i=0;i<(empty-dd);i++)
					{
						$('<div class="empty">&nbsp;</div>').appendTo($('div.cal'+id+'').find('.days'));
					}
			}
			else if(empty-dd==0){}
			else
			{
				for(var i=0;i<((empty-dd)+7);i++)
					{
						$('<div class="empty">&nbsp;</div>').appendTo($('div.cal'+id+'').find('.days'));
					}
			}
			
				
				if(count == 1)
					{
						for(var i=0; i<31; i++)
							{
								$('<div>'+(i+1)+'</div>').appendTo($('div.cal'+id+'').find('.days'));
							}
					}
					
				if(count == 2)
					{
						for(var i=0; i<28; i++)
							{
								$('<div>'+(i+1)+'</div>').appendTo($('div.cal'+id+'').find('.days'));
							}
					}
					
				if(count == 0)
					{
						for(var i=0; i<30; i++)
							{
								$('<div>'+(i+1)+'</div>').appendTo($('div.cal'+id+'').find('.days'));
							}
					}
					
				if(count == 3)
					{
						for(var i=0; i<29; i++)
							{
								$('<div>'+(i+1)+'</div>').appendTo($('div.cal'+id+'').find('.days'));
							}
					}
					
				var l = $('div.cal'+id+'').find('.days').find('div');
					for(var i=0; i<l.length; i++)
						{
							i+=6;
							$('<div class="clear"></div>').insertAfter($('div.cal'+id+'').find('.days').find('div:eq('+i+')'));
							i+=1;
						}
				
				clickbut();
				close();
				disable(id,mon,year);
				selectday();
				
			}			
			
		
		function clickbut()	
			{
				$('#container').find('.rentcar_book').find('div.cal').find('img#next').click(function()
																					{
																						var selcal = $(this).parent().parent().parent().attr('class').split(' ');
																						
																						var currmon = $('div.'+selcal[1]+'').find('.head').find('#mon'+selcal[1][3]+'').html();
																						var curryear = $('div.'+selcal[1]+'').find('.head').find('#year'+selcal[1][3]+'').html();
																						
																						var dn = $('div.'+selcal[1]+'').find('.days').find('div').last().index();
																						var cldiv = $('div.'+selcal[1]+'').find('.days').find('div.clear');
																						var dnindex = ((dn-cldiv.length)%7)+1;
																						
																						var h;
																						
																						switch (dnindex)
																							{
																								case 0:
																									h = 'Mon';
																								break;
																								
																								case 1:
																									h = 'Tue';
																								break;
																								
																								case 2:
																									h = 'Wed';
																								break;
																								
																								case 3:
																									h = 'Thu';
																								break;
																								
																								case 4:
																									h = 'Fri';
																								break;
																								
																								case 5:
																									h = 'Sat';
																								break;
																								
																								case 6:
																									h = 'Sun';
																								break;																								
																							}
																							
																						switch (currmon)
																							{
																								case 'Jan':
																									currmon = 'Feb';
																								break;
																								
																								case 'Feb':
																									currmon = 'Mar';
																								break;
																								
																								case 'Mar':
																									currmon = 'Apr';
																								break;
																								
																								case 'Apr':
																									currmon = 'May';
																								break;
																								
																								case 'May':
																									currmon = 'Jun';
																								break;
																								
																								case 'Jun':
																									currmon = 'Jul';
																								break;
																								
																								case 'Jul':
																									currmon = 'Aug';
																								break;
																								
																								case 'Aug':
																									currmon = 'Sep';
																								break;
																								
																								case 'Sep':
																									currmon = 'Oct';
																								break;
																								
																								case 'Oct':
																									currmon = 'Nov';
																								break;
																								
																								case 'Nov':
																									currmon = 'Dec';
																								break;
																								
																								case 'Dec':
																									currmon = 'Jan';
																									curryear++;																									
																								break;
																							}
																							
																						//alert(dnindex+' '+h);
																						
																						creatcal(selcal[1][3],currmon,1,h,curryear);
																						
																						//alert(currmon+' '+curryear);
																						
																					}
																				);
																				
				$('#container').find('.rentcar_book').find('div.cal').find('img#back').click(function()
																								{
																								
																									var selcal = $(this).parent().parent().parent().attr('class').split(' ');
																						
																									var currmon = $('div.'+selcal[1]+'').find('.head').find('#mon'+selcal[1][3]+'').html();
																									var curryear = $('div.'+selcal[1]+'').find('.head').find('#year'+selcal[1][3]+'').html();
																						
																									var dn = $('div.'+selcal[1]+'').find('.days').find('div[class="empty"]').last().index();
																									var dhn;
																									dn++;
																									var h;
																									if(dn == -1){dn=0;}
																									
																									switch (currmon)
																										{
																											case 'Jan':
																											
																												curryear = curryear-1;
																												currmon = 'Dec';
																												dhn = (31-dn)%7;
																												
																											break;
																											
																											case 'Feb':
																											
																												currmon = 'Jan';
																												dhn = (31-dn)%7;
																												
																											break;
																											
																											case 'Mar':
																											
																												currmon = 'Feb';
																												if((curryear%4)==0)
																													{
																														dhn = (29-dn)%7;
																													}
																												else
																													{
																														dhn = (28-dn)%7;
																													}																												
																												
																											break;
																											
																											case 'Apr':
																											
																												currmon = 'Mar';
																												dhn = (31-dn)%7;
																												
																											break;
																											
																											case 'May':
																											
																												currmon = 'Apr';
																												dhn = (30-dn)%7;
																												
																											break;
																											
																											case 'Jun':
																											
																												currmon = 'May';
																												dhn = (31-dn)%7;
																												
																											break;
																											
																											case 'Jul':
																											
																												currmon = 'Jun';
																												dhn = (30-dn)%7;
																												
																											break;
																											
																											case 'Aug':
																											
																												currmon = 'Jul';
																												dhn = (31-dn)%7;
																												
																											break;
																											
																											case 'Sep':
																											
																												currmon = 'Aug';
																												dhn = (31-dn)%7;
																												
																											break;
																											
																											case 'Oct':
																											
																												currmon = 'Sep';
																												dhn = (30-dn)%7;
																												
																											break;
																											
																											case 'Nov':
																											
																												currmon = 'Oct';
																												dhn = (31-dn)%7;
																												
																											break;
																											
																											case 'Dec':
																											
																												currmon = 'Nov';
																												dhn = (30-dn)%7;
																												
																											break;
																											
																										}
																										
																									//alert(dn);
																									
																									if(dhn == 1){h='Sun';}
																									if(dhn == 2){h='Sat';}
																									if(dhn == 3){h='Fri';}
																									if(dhn == 4){h='Thu';}
																									if(dhn == 5){h='Wed';}
																									if(dhn == 6){h='Tue';}
																									if(dhn == 7){h='Mon';}
																									
																									creatcal(selcal[1][3],currmon,1,h,curryear);
																									
																								}
																							);
																				
			}
		
		
		function selectday()
			{
				$('#container').find('.rentcar_book').find('div.cal').find('div.days').find('div[class!="empty"]').click(function()
																													{
																													
																														if($(this).attr('class')=='dis'){alert('error');}
																														else	
																														{
																														
																														var calid = $(this).parent().parent().attr('class').split(' ');
																														var ind = $(this).index();
																														var daynamesel;
																														if(ind>7 && ind<=14)
																															{
																																ind = ind-1-7;
																															}
																															
																														if(ind>15 && ind<=22)
																															{
																																ind = ind-2-14;
																															}
																															
																														if(ind>23 && ind<=30)
																															{
																																ind = ind-3-21;
																															}
																															
																														if(ind>31 && ind<=38)
																															{
																																ind = ind-4-28;
																															}
																															
																														if(ind>39 && ind<=46)
																															{
																																ind = ind-5-35;
																															}
																														
																														switch (ind+1)
																															{
																																case 1:
																																	daynamesel = 'Mon';
																																break;
																																
																																case 2:
																																	daynamesel = 'Tue';
																																break;
																																
																																case 3:
																																	daynamesel = 'Wed';
																																break;
																																
																																case 4:
																																	daynamesel = 'Thu';
																																break;
																																
																																case 5:
																																	daynamesel = 'Fri';
																																break;
																																
																																case 6:
																																	daynamesel = 'Sat';
																																break;
																																
																																case 7:
																																	daynamesel = 'Sun';
																																break;
																															}
																														
																														var sday = daynamesel+' '+$(this).html()+' '+$(this).parent().parent().find('.head').find('#mon'+calid[1][3]+'').html()+' '+$(this).parent().parent().find('.head').find('#year'+calid[1][3]+'').html()+' ';
																														
																														$('#date'+calid[1]+'').val(sday);
																														
																														$(this).parent().parent().fadeOut();
																														
																														}
																														
																													}
																												);
			}
		
		function disable(id,month,yr)
			{
				//alert(month+' '+yr);
				//alert(nowmon+' '+nowyear);
				if(month == nowmon && yr == nowyear)
					{
						var b = nowday-1+2;
						for(var i=1; i<=b;i++)
							{
								var a = $('div.cal'+id+'').find('.days').find('div[class!="empty"]');
								for(var j=0;j<a.length;j++)
									{
										if($(a[j]).html() == ''+i+'')
											{
												$(a[j]).attr('class','dis');
											}
									}
							}
					}
				
			}
		
		/*******************************************   calender end   ****************************************************/	
		
		var v;
		$('#container').find('.rentcar_book').find('input[type|=text]').focusin(function()
																				{
																					v = $(this).val();
																					if($(this).attr('readonly'))
																						{}
																					else
																						{
																							$(this).val('');
																						}
																				}
																			);
		
		$('#container').find('.rentcar_book').find('input[type|=text]').focusout(function()
																					{
																						if($(this).val() == '')
																							{
																								$(this).val(v);
																							}
																					}
																			);
	
	}
);