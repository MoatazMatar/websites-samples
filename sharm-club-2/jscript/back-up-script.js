$(function(){

	$('a').click(function(){

		var link_src = $(this).attr('href');

		if(link_src == '' || link_src == ' ' || link_src == '#'){return false;}

	});

	

	$('a#close_pop').click(function(){

		$(this).parent().slideUp();

		$('div#screen').slideUp();

	});
	
	/**tabs calender**/
	
	$('input#flights_dep').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 pair: $('input#flights_ret'),
			 offset: [-150, 20],
			 default_position: 'below'
		 });
		 
	$('input#flights_ret').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			offset: [-150, 20],
			 default_position: 'below'
	  });
	  
	$('input#excursions_arrv').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 pair: $('input#excursions_day'),
			 offset: [-150, 20],
			 default_position: 'below'
		 });
		 
	$('input#excursions_day').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			offset: [-150, 20],
			 default_position: 'below'
	  });
	
	$('input#hotel_checkin').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 offset: [-150, 20],
			 default_position: 'below'
	  });
	
	$('input#transfer_arrival').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 offset: [-150, 20],
			 default_position: 'below'
	  });
	
	$('input#flight_depature_date').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 offset: [-150, 20],
			 default_position: 'below'
	  });
	
	$('input#hot_check_in_dat').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 offset: [-150, 20],
			 default_position: 'below'
	  });
	
	/**tabs calender end**/
	
	if($('div#booking').position()){
		for(var i=0; i<$(this).find('div.det').length; i++){
			$('input#tour_day_'+(i+1)+'').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 offset: [-150, 20],
			 default_position: 'below'
			});
		}
		
		$('input#client_arrv').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 offset: [-150, 20],
			 default_position: 'below'
			});
	}
	
	if($('div#transfer').position()){
		$('input#client_arrv').Zebra_DatePicker({
			 first_day_of_week: 0,
			 format: 'D m, d, Y',
			 direction: 2,
			 disabled_dates: ['* * * 0,1'],
			 offset: [-150, 20],
			 default_position: 'below'
			});
	}
	
	// $('div#login').find('a').click(function(){
		// $('div#screen').slideDown();
		// $('div#screen_over').find('div.active').removeClass('active');
		// $('div#screen_over').slideDown();
		// $('div#screen_over').find('div.login').addClass('active');
		// $('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);
	// });
	
	if($('div.login').position()){
		$('div.login').find('li').find('span').click(function(){
			if($(this).parent().hasClass('active')){}
			else{
				$(this).parent().parent().find('li.active').removeClass('active');
				$(this).parent().addClass('active');
				$(this).parent().parent().parent().find('div.active').removeClass('active');
				$(this).parent().parent().parent().find('div.'+$(this).parent().attr('id')+'').addClass('active');
			}
		});
	}
	
	if($('div#login_page').position()){
		$('div#login_page').find('li').find('span').click(function(){
			if($(this).parent().hasClass('active')){}
			else{
				$(this).parent().parent().find('li.active').removeClass('active');
				$(this).parent().addClass('active');
				$(this).parent().parent().parent().find('div.active').removeClass('active');
				$(this).parent().parent().parent().find('div.'+$(this).parent().attr('id')+'').addClass('active');
			}
		});
	}
	
	$('div#screen').click(function(){
		$('div#screen_over').slideUp();
		$('div#screen').slideUp();
	});

	

	$(window).scroll(function(){

		var banner_menu_height = $('header').outerHeight(true)+$('#menu_holder').outerHeight(true);

		if($(window).scrollTop() > banner_menu_height){

			if($('#menu_holder').hasClass('top')){}

			else {

				$('#menu_holder').addClass('top');

				$('#tabs').addClass('top');

			}

		}

		else{

			$('#menu_holder').removeClass('top');

			$('#tabs').removeClass('top');

		}

	});

	

	$('#menu_holder').find('div.menu').find('li').hover(function(){

		//$('#menu_holder').find('div.menu').find('li').find('ul').hide();

		if($(this).find('ul').offset())

		{

			$(this).find('ul').fadeIn('slow');

		}

		else{}

	},function() {$(this).find('ul').hide();});
	
	$('#menu_holder').find('div.menu').find('li.mail').click(function(){
		$('div#screen').slideDown();
		$('div#screen_over').find('div.active').removeClass('active');
		$('div#screen_over').slideDown();
		$('div#screen_over').find('div.mail').addClass('active');
		$('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);
	});
	
	$('#menu_holder').find('div.menu').find('li.cart').click(function(){
		$('div#screen').slideDown();
		$('div#screen_over').find('div.active').removeClass('active');
		$('div#screen_over').slideDown();
		$('div#screen_over').find('div.cart').addClass('active');
		$('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);
	});

	

	if($('div#slider').position()){

		/*continue on this*/

		//$('div#slider').find('img').attr('style','min-height:'+($('div#header').outerHeight(true)+$('div#menu_holder').outerHeight(true)+$(window).outerHeight(true))+'');

		

		//$('div#slider').attr('style','min-height:'+($('div#slider').find('img').outerHeight(true) - ($('header').outerHeight(true)+$('div#menu_holder').outerHeight(true)))+'px');

		// $('div#slider').height($('div#slider').find('img').outerHeight(true) - ($('header').outerHeight(true)+$('div#menu_holder').outerHeight(true)));

		

		// var tabs_height = $('div#slider').find('img').outerHeight(true)-$('div#tabs').outerHeight(true)-10;

		var tabs_height = $('header').outerHeight(true)+$('div#menu_holder').outerHeight(true)+$('div#slider').outerHeight(true)-$('div#tabs').outerHeight(true)-50;

		$('div#tabs').attr('style','margin-top:-'+($('div#tabs').outerHeight(true)+20)+'px;');

	}

	

	$('div#tabs').find('ul').find('li').click(function(){

		if($(this).hasClass('active')){}

		else{

			$(this).parent().find('li.active').removeClass('active');

			$(this).parent().parent().find('div.item.active').removeClass('active');

			$(this).addClass('active');

			$(this).parent().parent().find('div.item.'+$(this).attr('id')+'').addClass('active');

		}

	});

	

	if($('div#tabs').position()){

		$('div#tabs').find('div.item.tab3').find('select[name=transfer_type]').change(function(){

			$('div.item.tab3').find('div.form_item:eq(1)').find('div.option.active').removeClass('active');

			$('div.item.tab3').find('div.form_item:eq(1)').find('div.option:eq('+($(this).val()-1)+')').addClass('active');

		});

	}

	

	if($('div#tour').position()){

		var x = $('div#tour').find('div.tour_desc').outerHeight(true);

		var y = $('div#tour').find('div.tour_det').outerHeight(true);

		if(x < y){$('div#tour').find('div.tour_desc').outerHeight(y-10);}

		else if(y<x){$('div#tour').find('div.tour_det').outerHeight(x-10);}

		else{}

		

		$('div.tour_tabs').find('ul#main_tour_tab').find('li').click(function(){

			if($(this).hasClass('active') || $(this).attr('id') == 'tour_tab_value'){}

			else{

				$(this).parent().find('li.active').removeClass('active');

				$(this).addClass('active');

				$(this).parent().find('li[id=tour_tab_value]').find('span').text($(this).text());

				$(this).parent().parent().find('div.active').removeClass('active');

				$(this).parent().parent().find('div.'+$(this).attr('id')+'').addClass('active');

			}

		});

	}

	

	if($('div#hotel').position()){

		var x = $('div#hotel').find('div.hotel_left_side').outerHeight(true);

		var y = $('div#hotel').find('div.hotel_right_side').outerHeight(true);

		if(x < y){$('div#hotel').find('div.hotel_left_side').outerHeight(y);}

		else if(y<x){$('div#hotel').find('div.hotel_right_side').outerHeight(x);}

		else{}

	}

	

	if($('div#excur').position()){

		var x = $('div#excur').find('div.excur_left_side').outerHeight(true);

		var y = $('div#excur').find('div.excur_right_side').outerHeight(true);

		if(x < y){$('div#excur').find('div.excur_left_side').outerHeight(y);}

		else if(y<x){$('div#excur').find('div.excur_right_side').outerHeight(x);}

		else{}

	}

	

	if($('div#all_article').position()){

		// var x = $('div#all_article').find('div.article_left_side').height();

		// var y = $('div#all_article').find('div.article_right_side').height();

		// if(x < y){$('div#all_article').find('div.article_left_side').height(y);}

		// else if(y<x){$('div#all_article').find('div.article_right_side').height(x);}

		// else{}

		

		$('div#all_article').find('div.article_left_side').find('div.art_left_item').find('h3').find('span:eq(0)').click(function(){

			$(this).parent().toggleClass('active');

			$(this).parent().parent().find('div').toggleClass('active');

		});

	}

	

	if($('div#account').position()){

		$('div.account_left_side').find('ul').find('li').find('span:eq(0)').click(function(){

			$(this).parent().toggleClass('active');

		});

		
/**********************************************************************************************************/
		$('div.account_right_side').find('ul').find('li#password').find('a').click(function(){

			/*$('div#screen').outerHeight($('body').outerHeight(true)+10);

			$('div#screen').slideDown();

			$('div#change_password').slideDown();

			$('div#change_password').css('margin-left',($('body').outerWidth(true)/2)-200);*/
			
			$('div#screen').slideDown();
			$('div#screen_over').find('div.active').removeClass('active');
			$('div#screen_over').slideDown();
			$('div#screen_over').find('div.account_password').addClass('active');
			$('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);

		});

		

		$('div.account_right_side').find('ul').find('li#profile').find('a').click(function(){

			/*$('div#screen').outerHeight($('body').outerHeight(true)+10);

			$('div#screen').slideDown();

			$('div#edit_profile').slideDown();

			$('div#edit_profile').css('margin-left',($('body').outerWidth(true)/2)-200);*/
			
			$('div#screen').slideDown();
			$('div#screen_over').find('div.active').removeClass('active');
			$('div#screen_over').slideDown();
			$('div#screen_over').find('div.account_profile').addClass('active');
			$('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);

		});

	}

	

	if($('div#booking').position()){

		$('div.book_item').find('label.title').find('input[type=checkbox]').change(function(){

			if($(this).parent().hasClass('active')){

				$(this).parent().removeClass('active');

				// $(this).parent().parent().find('div.det').removeClass('active');

				$(this).parent().next('div.det').removeClass('active');

			}

			else{

				$(this).parent().addClass('active');

				// $(this).parent().parent().find('div.det').addClass('active');

				$(this).parent().next('div.det').addClass('active');

			}

		});

		

		$('div.book_item').find('select[name=adult_num]').change(function(){

			var x = $(this).val();

			var intial = '<label>name</label><input type="text" /><br />';

			if(x == '1'){

				$(this).parent().find('div:eq(0)').html(intial);

			}

			else{

				var rest = '';

				for(var i=1; i<x; i++)

				{

					rest += '<label> &nbsp; </label><input type="text" /><br />';

				}

				$(this).parent().find('div:eq(0)').html(intial+' '+rest);

			}

		});

		

		$('div.book_item').find('select[name=child_num]').change(function(){

			var x = $(this).val();

			var intial = '<label>name</label><input type="text" /><br />';

			if(x == '0'){

				$(this).parent().find('div:eq(1)').html(' ');

			}

			else{

				var rest = '';

				for(var i=1; i<x; i++)

				{

					rest += '<label> &nbsp; </label><input type="text" /><br />';

				}

				$(this).parent().find('div:eq(1)').html(intial+' '+rest);

			}

		});

	}

	/**mobile version begin**/
	if($(window).outerWidth() < 1200){
		$('body').addClass('m_test');
		$('div#menu_holder').addClass('m_test');
		
		var meta_tag = '<meta name=viewport content="width=device-width, initial-scale=1">';
		
		$(meta_tag).appendTo('head');
		
		/**mobile menu begin**/
		var mob_menu = '<h2 id="mob_menu"><span><i class="fa fa-bars"></i>menu</span></h2>';
		
		var mob_login = '<h2 id="mob_login"><span><i class="fa fa-user"></i> <a href="'+$('div#login').find('a').attr('href')+'">login</a></span></h2>';
		
		var mob_cart = '<h2 id="mob_cart"><span><i class="fa fa-shopping-cart"></i> cart</span></h2>';
		
		var mob_menu_body = '<div id="mobile_menu_body"><div class="menu_close">x</div><div class="menu">'+$('div#menu_holder').find('div.menu').html()+'</div><div class="menu_close">x</div><div class="clear"></div></div><div class="clear"></div>';
		
		$(mob_menu+mob_cart+mob_login).insertBefore('div#menu_holder > div.menu');
		
		$(mob_menu_body).insertAfter('div#menu_holder');
		
		// $('div#mobile_menu_body').find('div.menu').attr('style',' ');
		
		$('div#menu_holder').find('div.menu').hide();
		$('div#tabs').attr('style',' ');
		
		$('h2#mob_menu').click(function(){
			if($('div#mobile_menu_body').offset()){
				if($('div#menu_holder').hasClass('top')){
					$('div#mobile_menu_body').addClass('top');
					$('div#mobile_menu_body').toggle();
				}
				else{
					$('div#mobile_menu_body').toggle();
				}
			}
			
			$('div#mobile_menu_body').find('div.menu_close').click(function(){
				$('div#mobile_menu_body').slideUp();
			});
			
			$('div#mobile_menu_body').find('div.menu').find('li').hover(function(){
				if($(this).find('ul').offset()){
					$(this).find('ul').slideDown();
				}
				else{}
			},function(){
				if($(this).find('ul').position()){
					$(this).find('ul').hide();
				}
				else{}
			});
			
			$('div#mobile_menu_body').find('div.menu').find('li.cart').click(function(){
				$('div#mobile_menu_body').slideUp();
				$('div#screen').slideDown();
				$('div#screen_over').find('div.active').removeClass('active');
				$('div#screen_over').slideDown();
				$('div#screen_over').find('div.cart').addClass('active');
				$('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);
			});
			
			$('div#mobile_menu_body').find('div.menu').find('li.mail').click(function(){
				$('div#screen').slideDown();
				$('div#screen_over').find('div.active').removeClass('active');
				$('div#screen_over').slideDown();
				$('div#screen_over').find('div.mail').addClass('active');
				$('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);
			});
			
		});
		
		$('h2#mob_cart').click(function(){
			$('div#screen').slideDown();
			$('div#screen_over').find('div.active').removeClass('active');
			$('div#screen_over').slideDown();
			$('div#screen_over').find('div.cart').addClass('active');
			$('div#screen_over').css('left',($(window).outerWidth(true)-$('div#screen_over').outerWidth(true))/2);
		});
		
		/**mobile menu end**/
		
		/**tabs menu begin**/
		
		var tabs_select = '<form id="book_select" action="#" name=""><label>select booking type</label><select><option id="tab1">Flights</option> <option id="tab2">Hotels</option> <option id="tab3">Transfers</option> <option id="tab4">Nile Cruise</option> <option id="tab5">Excursions</option></select></form>';
		
		$('div#tabs').find('ul').hide();
		$(tabs_select).insertAfter('div#tabs > ul');
		
		$('form#book_select').find('select').change(function(){
			$(this).parent().parent().find('div.item.active').removeClass('active');
			$(this).parent().parent().find('div.item.'+$(this).children(':selected').attr('id')+'').addClass('active');
		});
		
		/**tabs menu end**/
		
		
	
	}
	
	
	
	/**mobile version end**/
});