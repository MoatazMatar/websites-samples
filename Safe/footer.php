<footer>
	<div class="foot-hold">
		<div class="foot-hold-item address">
			<p>
				&copy; 2015 SafeHearts Co.<br />Office No. 14, 2A New Nasr City buildings<br />Al-Othman St. - Al Wafaa Wi Al Amal from Hassan Ma'moun ext. st.<br />Nasr City, Cairo, Egypt.<br /><br />Tel.: +202 29284050 - +20102 3336780 - +20100 5006922<br /> E-mail: info@safehearts.co - sales@safehearts.co
			</p>
		</div>
		<div class="foot-hold-item links">
			<h3>partners</h3>
			<ul>
				<li><a href="#" title="">icumed</a></li>
				<li><a href="#" title="">novarous</a></li>
				<li><a href="#" title="">silvermed</a></li>
				<li><a href="#" title="">avmed</a></li>
				<li><a href="#" title="">mec</a></li>
				<li><a href="#" title="">rein</a></li>
				<li><a href="#" title="">saniswiss</a></li>
			</ul>
		</div>
		<div class="foot-hold-item links">
			<h3>company</h3>
			<ul>
				<li><a href="#" title="">about us</a></li>
				<li><a href="#" title="">profile</a></li>
				<li><a href="#" title="">team work</a></li>
				<li><a href="#" title="">gallery</a></li>
				<li><a href="#" title="">careers</a></li>
				<li><a href="#" title="">contact us</a></li>
				<li><a href="#" title="">sitemap</a></li>
			</ul>
		</div>
		<div class="foot-hold-item links">
			<h3>follow</h3>
			<ul class="social">
				<li><a href="#" title=""><span class="hidden none">follow us on twitter</span><i class="fa fa-twitter"></i></a></li>
				<li><a href="#" title=""><span class="hidden none">follow us on facebook</span><i class="fa fa-facebook"></i></a></li>
				<li><a href="#" title=""><span class="hidden none">follow us on google+</span><i class="fa fa-google-plus"></i></a></li>
				<li><a href="#" title=""><span class="hidden none">watch us on youtube</span><i class="fa fa-youtube"></i></a></li>
				<li><a href="#" title=""><span class="hidden none">follow us on linkedin</span><i class="fa fa-linkedin"></i></a></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
</footer>