<!DOCTYPE html>
<html lang="en">
<head>
	<title>SafeHearts Co. | Medical Supplies</title>
	<meta charset="UTF-8" />
	<meta name=viewport content="width=device-width, initial-scale=1" />
	<meta name="description" content="We specializes in importing, manufacturing and distributing supplies in Interventional Cardiology, General surgery, Bariatric Interventional ,Infection control , Anesthesia, ICU ,Cardiac Surgery and Dialysis."/>
	<meta name="keywords" content="medical supplies, General surgery, Bariatric Interventional ,Infection control , Anesthesia, ICU ,Cardiac Surgery and Dialysis"/>
	<link type="image/jpg" href="img/safe-hearts-logo.jpg" rel="shortcut icon" />
	<meta name="google-site-verification" content="9Vo7gtVU7oXBByoC8h_RDilDseU0bHTDMHlCQBdrdVI" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="font-awesome-4.3.0/css/font-awesome.css" type="text/css" />
	<script src="jq/jq.js"></script>
	<script src="jq/script.js" async></script>	
		
	 <script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-70663822-1', 'auto');
		// ga('send', 'pageview');
	</script>
	
	<script type="application/ld+json"> { "@context": "http://schema.org", "@type": "MedicalOrganization", "name": "SafeHearts", "url": "http://www.safehearts.co", "logo": "http://www.safehearts.co/img/safe-hearts-logo.jpg", "contactPoint": [{ "@type": "ContactPoint", "telephone": "+20-229284050", "contactType": "customer service" }, { "@type": "ContactPoint", "telephone": "+20-1005006922", "contactType": "sales" }], "sameAs": ["https://www.facebook.com/SafeHeartsCo"], "address": { "@type": "PostalAddress", "streetAddress": "2A New Nasr City Buildings – Al-Othman St. – Al-Wafaa We Al-Amal St. From Hassan Ma’amoun ST. Ext.", "addressLocality": "Nasr City", "addressRegion": "Cairo - Egypt" }, "currenciesAccepted":"EGP, USD", "paymentAccepted":"Cash, credit card, VISA, JCB, Master Card" , "email":"info@safehearts.co"} </script>


	<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine0/style.css" />
	<script type="text/javascript" src="engine0/jquery.js"></script>
	<!-- End WOWSlider.com HEAD section -->

</head>

<body>
	<?php include 'header.php';?>
	<main>
		<section class="slider">
			<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
			<div id="wowslider-container0">
				<div class="ws_images">
					<ul>
						<li><img src="data0/images/465022623.jpg" alt="SafeHearts" title="SafeHearts" id="wows0_0"/>we can help to be safe</li>
						<li><img src="data0/images/childdentistrybanner.jpg" alt="jquery slider" title="SafeHearts" id="wows0_1"/>we can help to be safe</li>
						<li><img src="data0/images/depressedgirl_759x422_thinkstockphotos537248683.jpg" alt="SafeHearts" title="SafeHearts" id="wows0_2"/>we can help to be safe</li>
					</ul>
				</div>
				<div class="ws_bullets">
					<div>
						<a href="#" title="SafeHearts"><span><img src="data0/tooltips/465022623.jpg" alt="SafeHearts"/></span></a>
						<a href="#" title="SafeHearts"><span><img src="data0/tooltips/childdentistrybanner.jpg" alt="SafeHearts"/></span></a>
						<a href="#" title="SafeHearts"><span><img src="data0/tooltips/depressedgirl_759x422_thinkstockphotos537248683.jpg" alt="SafeHearts"/></span></a>
					</div>
				</div>
				<div class="ws_shadow"></div>
			</div>	
			<script type="text/javascript" src="engine0/wowslider.js"></script>
			<script type="text/javascript" src="engine0/script.js"></script>
			<!-- End WOWSlider.com BODY section -->
		</section>
		<section class="news">
			<h2>latest news:</h2>
			<ul>
				<marquee height="22px" direction="up" behavior="scroll" scrollamount="4" scrolldelay="200" truespeed="10" behavior="alternate">
				<li>news one title (head).</li>
				<li><a href="#" title="">news two title (head).</a></li>
				<li><a href="#" title="">news three title (head).</a></li>
				<li><a href="#" title="">news four title (head).</a></li>
				<li><a href="#" title="">news five title (head).</a></li>
				</marquee>
			</ul>
		</section>
		<section class="cols">
			<div class="item big">
				<h1>safehearts co.?</h1>
				<p>some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts, some brillant text about safehearts...<a href="#" title="">read more</a></p>
			</div>
			<div class="item">
				<h2>section title (solution title)</h2>
				<p>some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, </p>
				<h3>products</h3>
				<ul>
					<li><a href="#" title="">product one title.</a></li>
					<li><a href="#" title="">product two title.</a></li>
					<li><a href="#" title="">product three title.</a></li>
					<li><a href="#" title="">product four title.</a></li>
				</ul>
			</div>
			<div class="item">
				<h2>section title (solution title)</h2>
				<p>some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, </p>
				<h3>products</h3>
				<ul>
					<li><a href="#" title="">product one title.</a></li>
					<li><a href="#" title="">product two title.</a></li>
					<li><a href="#" title="">product three title.</a></li>
					<li><a href="#" title="">product four title.</a></li>
				</ul>
			</div>
			<div class="item">
				<h2>section title (solution title)</h2>
				<p>some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, </p>
				<h3>products</h3>
				<ul>
					<li><a href="#" title="">product one title.</a></li>
					<li><a href="#" title="">product two title.</a></li>
					<li><a href="#" title="">product three title.</a></li>
					<li><a href="#" title="">product four title.</a></li>
				</ul>
			</div>
			<div class="item">
				<h2>section title (solution title)</h2>
				<p>some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, </p>
				<h3>products</h3>
				<ul>
					<li><a href="#" title="">product one title.</a></li>
					<li><a href="#" title="">product two title.</a></li>
					<li><a href="#" title="">product three title.</a></li>
					<li><a href="#" title="">product four title.</a></li>
				</ul>
			</div>
			<div class="item">
				<h2>section title (solution title)</h2>
				<p>some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, </p>
				<h3>products</h3>
				<ul>
					<li><a href="#" title="">product one title.</a></li>
					<li><a href="#" title="">product two title.</a></li>
					<li><a href="#" title="">product three title.</a></li>
					<li><a href="#" title="">product four title.</a></li>
				</ul>
			</div>
			<div class="item">
				<h2>section title (solution title)</h2>
				<p>some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, some text about this section or solution, </p>
				<h3>products</h3>
				<ul>
					<li><a href="#" title="">product one title.</a></li>
					<li><a href="#" title="">product two title.</a></li>
					<li><a href="#" title="">product three title.</a></li>
					<li><a href="#" title="">product four title.</a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</section>
		<section class="partners">
			<div class="partner-item">
				<a href="#" title=""><img src="img/foot-logo.png" title="" alt="" /></a>
				<h2><a href="#" title="">partner name</a></h2>
			</div>
			<div class="partner-item">
				<a href="#" title=""><img src="img/foot-logo.png" title="" alt="" /></a>
				<h2><a href="#" title="">partner name</a></h2>
			</div>
			<div class="partner-item">
				<a href="#" title=""><img src="img/foot-logo.png" title="" alt="" /></a>
				<h2><a href="#" title="">partner name</a></h2>
			</div>
			<div class="partner-item">
				<a href="#" title=""><img src="img/foot-logo.png" title="" alt="" /></a>
				<h2><a href="#" title="">partner name</a></h2>
			</div>
			<div class="partner-item">
				<a href="#" title=""><img src="img/foot-logo.png" title="" alt="" /></a>
				<h2><a href="#" title="">partner name</a></h2>
			</div>
			<div class="partner-item row-last">
				<a href="#" title=""><img src="img/foot-logo.png" title="" alt="" /></a>
				<h2><a href="#" title="">partner name</a></h2>
			</div>
		</section>			
	</main>
	<?php include 'footer.php';?>
</body>
</html>