<!DOCTYPE html>
<html lang="en">
<head>
	<title>Products</title>
	<meta charset="UTF-8" />
	<meta name=viewport content="width=device-width, initial-scale=1" />
	<meta name="description" content="We specializes in importing, manufacturing and distributing supplies in Interventional Cardiology, General surgery, Bariatric Interventional ,Infection control , Anesthesia, ICU ,Cardiac Surgery and Dialysis."/>
	<meta name="keywords" content="medical supplies, General surgery, Bariatric Interventional ,Infection control , Anesthesia, ICU ,Cardiac Surgery and Dialysis"/>
	<link type="image/jpg" href="../img/safe-hearts-logo.jpg" rel="shortcut icon" />
	<meta name="google-site-verification" content="9Vo7gtVU7oXBByoC8h_RDilDseU0bHTDMHlCQBdrdVI" />
	<link href="../css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../font-awesome-4.3.0/css/font-awesome.css" type="text/css" />
	<script src="../jq/jq.js"></script>
	<script src="../jq/script.js" async></script>	
		
	 <script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-70663822-1', 'auto');
		// ga('send', 'pageview');
	</script>
	
	<script type="application/ld+json"> { "@context": "http://schema.org", "@type": "MedicalOrganization", "name": "SafeHearts", "url": "http://www.safehearts.co", "logo": "http://www.safehearts.co/img/safe-hearts-logo.jpg", "contactPoint": [{ "@type": "ContactPoint", "telephone": "+20-229284050", "contactType": "customer service" }, { "@type": "ContactPoint", "telephone": "+20-1005006922", "contactType": "sales" }], "sameAs": ["https://www.facebook.com/SafeHeartsCo"], "address": { "@type": "PostalAddress", "streetAddress": "2A New Nasr City Buildings � Al-Othman St. � Al-Wafaa We Al-Amal St. From Hassan Ma�amoun ST. Ext.", "addressLocality": "Nasr City", "addressRegion": "Cairo - Egypt" }, "currenciesAccepted":"EGP, USD", "paymentAccepted":"Cash, credit card, VISA, JCB, Master Card" , "email":"info@safehearts.co"} </script>
</head>

<body>
	<?php include 'header.php';?>
	<main>
		<section class="img">
			<img src="../img/about-image.jpg" title="" alt="" />
		</section>
		<section class="news">
			<ul class="path">
				<li><a href="#" title="">home</a></li>
				<li>|</li>
				<li><a href="#" title="">products</a></li>
			</ul>
		</section>
		<section class="products">
			<div class="sectors">
				<h2 class="title"><a href="#" title="">icu</a><span>+</span></h2>
				<p>
					some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, 
					some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector...<a href="#" title="">read more</a> 
				</p>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod end">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				
			</div>
			<div class="sectors">
				<h2 class="title"><a href="#" title="">seliver-med</a><span>+</span></h2>
				<p>
					some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, 
					some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector, some text about this sector...<a href="#" title="">read more</a> 
				</p>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod end">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				<div class="prod">
					<a href="#" title=""><img src="../img/safety-box.png" title="" alt="" /></a>
					<h2><a href="#" title="">product name</a></h2>
					<p>some description one the product, some description one the product, some description one the product, some description one the product</p>
				</div>
				
			</div>
			
		</section>
	</main>
	<?php include 'footer.php';?>
</body>
</html>