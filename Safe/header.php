<header>
	<div class="contact-info">
		<span><i class="fa fa-mobile"></i>contact us on: +2 02 2928 4050</span>
		<span><i class="fa fa-envelope"></i>info@safehearts.co</span>
	</div>
	<div class="logo">
		<div class="img"><img src="img/header-logo.png" alt="SafeHearts Co. Offical Logo" title="SafeHearts Co. For Medical Supplies"/></div>
		<div class="txt">
			<p>we can help you to be safe</p>
			<ul>
				<li><a href="#">careers</a></li>
				<li><a href="#">news &amp; events</a></li>
				<li><a href="#">contact us</a></li>
			</ul>
		</div>
	</div>
</header>
<nav>
	<div class="wide-menu">
		<ul>
			<li><a href="#" title=""><span class="hidden none">home</span><i class="fa fa-home"></i></a></li>
			<li><a href="#" title="">products</a></li>
			<li><a href="#" title="">solutions</a></li>
			<li><a href="#" title="">support</a></li>
			<li><a href="#" title="">services</a></li>
			<li><a href="#" title="">about us</a></li>
			<!-- <li><a href="#" title="">careers</a></li> -->
		</ul>
	</div>
	<div class="mob-menu">
		<p><i class="fa fa-bars"></i>menu</p>
		<ul>
			<li><a href="#" title="">home</a></li>
			<li><a href="#" title="">products</a></li>
			<li><a href="#" title="">solutions</a></li>
			<li><a href="#" title="">support</a></li>
			<li><a href="#" title="">services</a></li>
			<li><a href="#" title="">about us</a></li>
			<!-- <li><a href="#" title="">careers</a></li> -->
		</ul>
	</div>
</nav>