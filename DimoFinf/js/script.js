$(function(){
	"use strict";
	$('.gal-item').find('img').hover(function(){
		$(this).parent().find('div').slideDown(500);
	});
	$('.gal-item').find('div').find('div').mouseleave(function(){
		$(this).fadeOut('fast');
	});
	
	$('.tabs-menu').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			var lid = $(this).attr('id');
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$(this).parent().parent().find('div.content').find('div.active').removeClass('active');
			$(this).parent().parent().find('div.content').find('div.'+lid+'').addClass('active');
		}
	});
	$('.scrollTop').click(function(){
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 'slow', 'swing');
	});
	$('.DimofinfMenu').find('li').hover(function(){
		if($(this).find('ul.Submenu')){
			$(this).find('ul.Submenu').slideDown();
		}
	},function(){
		$(this).find('ul.Submenu').slideUp();
	});
	/*$('ul.Submenu').mouseleave(function(){
		$(this).hide();
	});*/
});