$(function(){
	"use strict";
	$('a').click(function(){
		if($(this).attr('href')==='#'){
			return false;
		}
	});
	
	$('ul.tab').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$(this).parent().parent().find('div.active').removeClass('active');
			var target_class = $(this).attr('id');
			if(target_class==='mission'){
				$(this).parent().parent().find('div:eq(0)').addClass('active');
			}
			else{
				$(this).parent().parent().find('div:eq(1)').addClass('active');
			}
		}
	});
	
	$('ul.pro-tab').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			var this_id = $(this).attr('id');
			$(this).parent().parent().find('div.active').removeClass('active');
			$(this).parent().parent().find('div.'+this_id+'').addClass('active');
		}
	});
});