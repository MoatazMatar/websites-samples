$(function()
	{
		var dest;
		
		$('.flight_only').find('td:first').find('input[type=radio][name=r0]').change(function()
																					{
																						//alert($('.flight_only').find('td:first').find('input[type=radio]').select().attr('title'));
																						dest = $('.flight_only').find('td:first').find('input[type=radio][name=r0]:checked').next('label').text();
																						sel(dest);
																					}
																				);
																				
		function sel(a)
			{
				if(a == 'airport - hotel')
					{
						var b = '<option>Sharm el Sheikh</option><option>Cairo</option><option>Hurghada</option><option>Luxor</option><option>Nuweiba</option><option>Taba</option>';
						$('.flight_only').find('select[name=from]').html(b);
						var c = '<option>Ras Om el Sid</option><option>Tower</option><option>Na�ama bay</option><option>Shark�s bay</option><option>Nabq bay</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if(a == 'hotel - hotel')
					{
						var b = '<option>Sharm el Sheikh</option><option>Cairo</option><option>Hurghada</option><option>Luxor</option>';
						$('.flight_only').find('select[name=from]').html(b);
						var c = '<option>Sharm el Sheikh</option><option>Cairo</option><option>Hurghada</option><option>Luxor</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if(a == 'hotel - place in the city')
					{
						var b = '<option>Sharm el Sheikh</option><option>Cairo</option><option>Hurghada</option><option>Luxor</option><option>el-gouna</option><option>makadi bay</option>';
						$('.flight_only').find('select[name=from]').html(b);
						var c = '<option>Mercato</option><option>Na�ama bay</option><option>Nabq Bay</option><option>Old Market</option><option>Soho square</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
			}
			
		$('.flight_only').find('select[name=from]').change(function()
																{
																	var d = $('.flight_only').find('select[name=from]').val();
																	selto(d);
																}
															);
															
		function selto(a)
			{
				if((dest == undefined || dest == 'airport - hotel') && a == 'Sharm el Sheikh')
					{
						var c = '<option>Ras Om el Sid</option><option>Tower</option><option>Na�ama bay</option><option>Shark�s bay</option><option>Nabq bay</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if((dest == undefined || dest == 'airport - hotel') && a == 'Cairo')
					{
						var c = '<option>Cairo</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if((dest == undefined || dest == 'airport - hotel') && a == 'Luxor')
					{
						var c = '<option>Luxor</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if((dest == undefined || dest == 'airport - hotel') && a == 'Nuweiba')
					{
						var c = '<option>Nuweiba</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if((dest == undefined || dest == 'airport - hotel') && a == 'Taba')
					{
						var c = '<option>Taba</option><option>Taba Heights</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if((dest == undefined || dest == 'airport - hotel') && a == 'Hurghada')
					{
						var c = '<option>Hurghada</option><option>El Gouna</option><option>Makadi Bay</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if(dest == 'hotel - hotel' && a == 'Sharm el Sheikh')
					{
						var c = '<option>Sharm el Sheikh</option><option>Cairo</option><option>Hurghada</option><option>Luxor</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
					
				if(dest == 'hotel - place in the city' && (a == 'Cairo' || a == 'Hurghada' || a == 'Luxor' || a =='el-gouna' || a == 'makadi bay' ))
					{
						var c = ' ';
						$('.flight_only').find('select[name=to]').html(' ');
					}
					
				if(dest == 'hotel - place in the city' && a == 'Sharm el Sheikh' )
					{
						var c = '<option>Mercato</option><option>Na�ama bay</option><option>Nabq Bay</option><option>Old Market</option><option>Soho square</option>';
						$('.flight_only').find('select[name=to]').html(c);
					}
				
			}
		
	}
);