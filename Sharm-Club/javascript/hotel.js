$(function()
	{
	
		$(".hotel_menu > li").click(function(e)
											{
												switch(e.target.id)
													{
														case "hotel_gallery":
															
															$("#hotel_gallery").addClass("active");
															$("#hotel_rate").removeClass("active");
															$("#hotel_map").removeClass("active");
															$("#hotel_preview").removeClass("active");
															
															$("div.hotel_gallery").fadeIn();
															$("div.hotel_rate").css("display","none");
															$("div.hotel_map").css("display","none");
															$("div.hotel_preview").css("display","none");
															
															$("#label span").html("gallery");
															
															break;
															
														case "hotel_rate":
															
															$("#hotel_rate").addClass("active");
															$("#hotel_gallery").removeClass("active");
															$("#hotel_map").removeClass("active");
															$("#hotel_preview").removeClass("active");
															
															$("div.hotel_rate").fadeIn();
															$("div.hotel_gallery").css("display","none");
															$("div.hotel_map").css("display","none");
															$("div.hotel_preview").css("display","none");
															
															$("#label span").html("rates");
															
															break;															
															
														case "hotel_map":
															
															$("#hotel_map").addClass("active");
															$("#hotel_gallery").removeClass("active");
															$("#hotel_rate").removeClass("active");
															$("#hotel_preview").removeClass("active");
															
															$("div.hotel_map").fadeIn();
															$("div.hotel_gallery").css("display","none");
															$("div.hotel_rate").css("display","none");
															$("div.hotel_preview").css("display","none");
															
															$("#label span").html("map");
															
															break;
															
														case "hotel_preview":
															
															$("#hotel_preview").addClass("active");
															$("#hotel_gallery").removeClass("active");
															$("#hotel_rate").removeClass("active");
															$("#hotel_map").removeClass("active");
															
															$("div.hotel_preview").fadeIn();
															$("div.hotel_gallery").css("display","none");
															$("div.hotel_rate").css("display","none");
															$("div.hotel_map").css("display","none");
															
															$("#label span").html("preview");
															
															break;
													}
											}
										);
										
										
		$('.images > img').click(function()
										{
																
											var $newpath = $(this).attr("src");
											var $newdes = $(this).attr("alt");
											$("#largeimg").attr({src:$newpath});
											$("#des").text($newdes);
																
										}
								);
								
		
		var m0 = $("#container #rate_num");
		for(var i=0 ;i<m0.length; i++)
			{
				var m = $(m0[i]).html();
				$(m0[i]).parent().find('img:eq('+(m-1)+')').css("display","block");
			}
			
		var m0 = $("#hotel_body").find("#hsearch_result").find('.hsearch_result_item div:eq(1)').find("#rate_num");
		for(var i=0 ;i<m0.length; i++)
			{
				var m = $(m0[i]).html();
				$(m0[i]).parent().find('img:eq('+(m-1)+')').css("display","block");
			}
			
			
	}

);