$(function()
	{
		$("#transfer_menu ul li").click(function(e)
											{
												switch(e.target.id)
													{
														case "tran_other":
															
															$("#tran_other").addClass("tran_active");
															$("#tran_city").removeClass("tran_active");
															$("#tran_hotel").removeClass("tran_active");
															$("#tran_air").removeClass("tran_active");
															
															$("div.tran_other").fadeIn();
															$("div.tran_city").css("display","none");
															$("div.tran_hotel").css("display","none");
															$("div.tran_air").css("display","none");
															
														break;
														
														case "tran_city":
														
															$("#tran_city").addClass("tran_active");
															$("#tran_other").removeClass("tran_active");
															$("#tran_hotel").removeClass("tran_active");
															$("#tran_air").removeClass("tran_active");
															
															$("div.tran_city").fadeIn();
															$("div.tran_other").css("display","none");
															$("div.tran_hotel").css("display","none");
															$("div.tran_air").css("display","none");
															
														break;
														
														case "tran_hotel":
														
															$("#tran_hotel").addClass("tran_active");
															$("#tran_other").removeClass("tran_active");
															$("#tran_city").removeClass("tran_active");
															$("#tran_air").removeClass("tran_active");
															
															$("div.tran_hotel").fadeIn();
															$("div.tran_other").css("display","none");
															$("div.tran_city").css("display","none");
															$("div.tran_air").css("display","none");
															
														break;
														
														case "tran_air":
														
															$("#tran_air").addClass("tran_active");
															$("#tran_other").removeClass("tran_active");
															$("#tran_hotel").removeClass("tran_active");
															$("#tran_city").removeClass("tran_active");
															
															$("div.tran_air").fadeIn();
															$("div.tran_other").css("display","none");
															$("div.tran_hotel").css("display","none");
															$("div.tran_city").css("display","none");
															
														break;
													}
													
												return false;
											}
										);
	}
);