$(function(){
	"use strict";
	
	function rejustfy(){
		if($('.partners-tabs').position()){
		var totimg = $('.partners-page').find('.partner');
		for(var i =0; i<totimg.length; i++){
			var imgheight = $('.partners-page').find('.partner:eq('+i+')').find('img').height();
			var itemheight = $('.partners-page').find('.partner:eq('+i+')').outerHeight(true);
			$('.partners-page').find('.partner:eq('+i+')').find('img').css("marginTop",(itemheight-imgheight)/2);
			$('.partners-page').find('.partner:eq('+i+')').find('img').css("marginBottom",(itemheight-imgheight)/2);
			$('.partners-page').find('.partner:eq('+i+')').find('.overlaytxt').css("marginBottom",(itemheight-160)/2);
			$('.partners-page').find('.partner:eq('+i+')').find('.overlaytxt').css("marginTop",(itemheight-160)/2);
			}
		}
	}
	
	$('a').click(function(){
		if($(this).attr('href')==='#'){return false;}
	});
	
	$('.tabs-menu').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$('.tabs-body').find('div.active').removeClass('active');
			$('.tabs-body').find('div.'+$(this).attr('id')+'').addClass('active');
		}
	});
	
	$('#nbottom').click(function(){
		var itemlen = $(this).parent().find('div.items').find('div.item');
		var itemind = $(this).parent().find('div.items').find('div.active.br').index();
		if(itemind === (itemlen.length-2)){
			$(this).parent().find('div.items').find('div.active.br').delay(850).removeClass('active').removeClass('br');
			$(this).parent().find('div.items').find('div.active').addClass('br');
			$(this).parent().find('div.items').find('div.active.br').next('div.item').addClass('active');
			$(this).addClass('disable');
		}
		else if(itemind === (itemlen.length-1)){}
		else{
			if($('#ntop').hasClass('disable')){$('#ntop').removeClass('disable');}
			$(this).parent().find('div.items').find('div.active.br').delay(850).removeClass('active').removeClass('br');
			$(this).parent().find('div.items').find('div.active').addClass('br');
			$(this).parent().find('div.items').find('div.active.br').next('div.item').addClass('active');
		}
		return false;
	});
	
	$('#ntop').click(function(){
		var itemind = $(this).parent().find('div.items').find('div.active.br').index();
		if(itemind === 1){
			$(this).parent().find('div.items').find('div.active.br').next('div.item.active').removeClass('active');
			$(this).parent().find('div.items').find('div.active.br').removeClass('br');
			$(this).parent().find('div.items').find('div.active').prev('div.item').addClass('active').addClass('br');
			$(this).addClass('disable');
		}
		else if(itemind === 0){}
		else{
			if($('#nbottom').hasClass('disable')){$('#nbottom').removeClass('disable');}
			$(this).parent().find('div.items').find('div.active.br').next('div.item.active').removeClass('active');
			$(this).parent().find('div.items').find('div.active.br').removeClass('br');
			$(this).parent().find('div.items').find('div.active').prev('div.item').addClass('active').addClass('br');
		}
		return false;
	});
	
	
	
	$(window).scroll(function(){
		if($('.pro').offset()){
			var scrolltop = $(window).scrollTop();
			var prooffset = $('.pro').position();
			var windowwidth = $(document).outerWidth(true);
			/*alert(windowwidth);*/
			if(scrolltop >= (prooffset.top-(prooffset.top/1.7))){
				$('.pro').find('.tit').addClass('animated').addClass('fadeInUp');
			}

			if(windowwidth){
				for(var i=0; i<4; i++){
					if(scrolltop >= (prooffset.top-(prooffset.top/2.2))){
						if(i%2 === 0){$('.pro').find('.item:eq('+i+')').addClass('animated').addClass('fadeInUpBig');}
						else {$('.pro').find('.item:eq('+i+')').addClass('animated').addClass('fadeInDownBig');}
					}
				}
				for(var i=4; i<8; i++){
					if(scrolltop >= (prooffset.top-(prooffset.top/3))){
						if(i%2 === 0){$('.pro').find('.item:eq('+i+')').addClass('animated').addClass('fadeInUpBig');}
						else {$('.pro').find('.item:eq('+i+')').addClass('animated').addClass('fadeInDownBig');}
					}
				}
			}
		}
		
		if($('.services-page').position()){
			var scrolltop = $(window).scrollTop();
			var servoffset = $('.services-panel').offset();
			var seritemheight = $('.services-panel').find('.seritem:eq(0)').outerHeight(true);
			if(scrolltop >= (servoffset.top-seritemheight)/6){
				$('.services-panel').find('.seritem:eq(1)').find('div.servleft').addClass('fadeInUp animated visible');
				$('.services-panel').find('.seritem:eq(1)').find('div.servright').addClass('fadeInUp animated visible');
			}
			if(scrolltop >= (servoffset.top-seritemheight)/2){
				$('.services-panel').find('.seritem:eq(2)').find('div.servleft').addClass('fadeInUp animated visible');
				$('.services-panel').find('.seritem:eq(2)').find('div.servright').addClass('fadeInUp animated visible');
			}
			if(scrolltop >= (servoffset.top-seritemheight)/2+350){
				$('.services-panel').find('.seritem:eq(3)').find('div.servleft').addClass('fadeInUp animated visible');
				$('.services-panel').find('.seritem:eq(3)').find('div.servright').addClass('fadeInUp animated visible');
			}
			if(scrolltop >= (servoffset.top-seritemheight)/2+650){
				$('.services-panel').find('.seritem:eq(4)').find('div.servleft').addClass('fadeInUp animated visible');
				$('.services-panel').find('.seritem:eq(4)').find('div.servright').addClass('fadeInUp animated visible');
			}
		}
		

//		if($('div.servProjects').position()){
//			var scrolltop = $(window).scrollTop();
//			var thistop = $('div.servProjects').position();
//			if(scrolltop >= thistop){
//				$('div.servProjects').css({
//					'position': 'fixed',
//					'top': '5px'
//				});
//			}
//		}

		
	});
	
	$('.partners-tabs').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$(this).parent().parent().find('div.partners-body-item.active').removeClass('active');
			$(this).parent().parent().find('div.'+$(this).attr('id')+'').addClass('active').delay(2000);
			rejustfy();
		}
	});
	
	if($('.partners-tabs').position()){
		var totimg = $('.partners-page').find('.partner');
		for(var i =0; i<totimg.length; i++){
			var imgheight = $('.partners-page').find('.partner:eq('+i+')').find('img').height();
			var itemheight = $('.partners-page').find('.partner:eq('+i+')').outerHeight(true);
			$('.partners-page').find('.partner:eq('+i+')').find('img').css("marginTop",(itemheight-imgheight)/2);
			$('.partners-page').find('.partner:eq('+i+')').find('img').css("marginBottom",(itemheight-imgheight)/2);
			$('.partners-page').find('.partner:eq('+i+')').find('.overlaytxt').css("marginBottom",(itemheight-160)/2);
			$('.partners-page').find('.partner:eq('+i+')').find('.overlaytxt').css("marginTop",(itemheight-160)/2);
		}
	}
	
	if($('.projects-page').position()){
		
		for(var i=0; i<2; i++){
			var projectscount = $('.projects-body').find('.pbcontent:eq('+i+')').find('.item');
			if(projectscount.length > 12){
				var pagehtmlinit = '<div class="pbpage">';
				var pagehtmlend = '</div>';
				var pagehtmlbody = '';
				for(var j=0; j<Math.ceil(projectscount.length/12); j++){
					if(j===0){pagehtmlbody+= '<span class="active">'+(j+1)+'</span>';}
					else{pagehtmlbody+= '<span>'+(j+1)+'</span>';}
					
				}
				$(pagehtmlinit+pagehtmlbody+pagehtmlend).appendTo($('.projects-body').find('.pbcontent:eq('+i+')'));
				for(var x=12; x<projectscount.length; x++){
					$('.projects-body').find('.pbcontent:eq('+i+')').find('.item:eq('+x+')').hide();
				}
			}
		}
		
		$('div.pbpage').find('span').click(function(){
			if($(this).hasClass('active')){}
			else{
				$(this).parent().find('span.active').removeClass('active');
				$(this).addClass('active');
				var num = $(this).text();
				switch(num){
					case "1":
						var projectscount = $(this).parent().parent().find('.projects-palet').find('.item');
						for(var x=0; x<12; x++){
								$(this).parent().parent().find('.projects-palet').find('.item:eq('+x+')').show();
							}
						for(var y=12; y<projectscount.length; y++){
								$(this).parent().parent().find('.projects-palet').find('.item:eq('+y+')').hide();
							}
						break;
					case "2":
						for(var x=0; x<12; x++){
								$(this).parent().parent().find('.projects-palet').find('.item:eq('+x+')').hide();
							}
						for(var y=12; y<24; y++){
								$(this).parent().parent().find('.projects-palet').find('.item:eq('+y+')').show();
							}
						break;
				}
			}
		});
		
		$('.projects-tabs').find('li').click(function(){
			if($(this).hasClass('active')){}
			else if($(this).hasClass('indeicator')){}
			else{
				$(this).parent().find('li.active').removeClass('active');
				$(this).addClass('active');
				$('.projects-body').find('div.pbcontent.active').removeClass('active');
				$('.projects-body').find('div.pbcontent.'+$(this).attr('id')+'').addClass('fadeInUp animated active');
			}
		});
		
		$('.projects-palet').find('div.item').mouseenter(function(){
			$(this).find('.overlaytxt').find('p').addClass('animated flipInX');
		});
	}
	
	$('.pro-grid').find('.item').click(function(){
		var itemimg = $(this).find('.proimg').find('img').attr('src');
		var itemtit = $(this).find('.protxt').find('h3').text();
		var itemtxt = $(this).find('.protxt').find('p').text();
		var itemindex = $(this).index();
	});
	
	$('.project-menu').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$('.project-body').find('div.active').removeClass('active');
			$('.project-body').find('div.'+$(this).attr('id')+'').addClass('active animated bounceInUp');
		}
	});
	
	$('.project-body').find('.img-small').find('img').click(function(){
		$('.img-big').find('img').attr('src',$(this).attr('src'));
	});
	
	
/*	if($('span.servicon').position()){
		var serviconcount = $('.services-panel').find('span.servicon');
		for(var i=0; i<serviconcount.length; i++){
			var parheight = $('span.servicon:eq('+i+')').parent().outerHeight(true);
			$('span.servicon:eq('+i+')').css({
				height:parheight/2,
				width:parheight/2,
				marginTop:parheight/4,
				marginBottom:parheight/4
			});
		}
	}*/
	
});
