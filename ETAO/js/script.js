$(function(){
	"use strict";
	
	$('.prod-arrow').find('i').click(function(){
		if($(this).hasClass('left')){
			var fristVisibleIndex = $('.home-products').find('.home-products-item.active').first().index();
			if(fristVisibleIndex === 0){
				$('.home-products').find('.animated.fadeInLeft').removeClass('animated fadeInLeft');
				$('.home-products').find('.animated.fadeInRight').removeClass('animated fadeInRight');
				$('.home-products').find('.home-products-item:eq(2)').removeClass('active');
				$('.home-products').find('.home-products-item').last().remove().prependTo('.home-products').addClass('active animated fadeInLeft');
			}
		}
		else{
			var lastVisibleIndex = $('.home-products').find('.home-products-item.active').last().index();
			if(lastVisibleIndex === 2){
				$('.home-products').find('.animated.fadeInLeft').removeClass('animated fadeInLeft');
				$('.home-products').find('.animated.fadeInRight').removeClass('animated fadeInRight');
				$('.home-products').find('.home-products-item:eq(0)').removeClass('active');
				$('.home-products').find('.home-products-item:eq(3)').addClass('active animated fadeInRight');
				$('.home-products').find('.home-products-item:eq(0)').remove().appendTo('.home-products');
//				$('.home-products').find('.home-products-item:eq(0)').removeClass('active');
//				$('.home-products').find('.home-products-item').last().remove().prependTo('.home-products').addClass('active animated fadeInLeft');
			}
		}
	});
	
	$('.clients-tabs').find('li').click(function(){
		console.log($('.home-clients-body-content').find('.'+$(this).attr('id')+''));
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			
			$('.home-clients-body-content').find('.active').removeClass('active');
			$('.home-clients-body-content').find('.'+$(this).attr('id')+'').addClass('active');
		}
	});
});
