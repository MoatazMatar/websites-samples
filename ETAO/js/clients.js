$(function(){
	"use strict";
	
	$('.clients-tab').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$('.clients-tabs-body > .active').removeClass('active');
			$('.clients-tabs-body > .'+$(this).attr('id')+'').addClass('active');
		}
	});
	
	$('.clients-tabs-body').find('ul.row').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).parent().parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$(this).parent().parent().find('div.client-body.active').removeClass('active');
			$(this).parent().parent().find('div.client-body.'+$(this).attr('id')+'').addClass('active');
//			$(this).parent().parent().find('div.client-body.'+$(this).attr('id')+'').find('div').addClass('active');
		}
	});
});
