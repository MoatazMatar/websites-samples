$(function(){
	"use strict";
	
	$('.about-tabs').find('li').click(function(){
		if($(this).hasClass('active')){}
		else{
			$(this).parent().find('li.active').removeClass('active');
			$(this).addClass('active');
			$('.about-tabs-body').find('div.active').removeClass('active');
			$('.about-tabs-body').find('div.'+$(this).attr('id')+'').addClass('active');
		}
	});
});
